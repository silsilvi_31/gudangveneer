@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>PERBANDINGAN</h2>
            <ul class="nav navbar-right panel_toolbox">
              
            {{-- <a href="{{route('bk_banding.form')}}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a> --}}
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="card-box table-responsive">
                  <table id="tbBk" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No Urut</th>
                        <th>Tanggal</th>
                        <th>BK</th>
                        {{-- <th>Jenis BK</th>
                        <th>Kepada</th>
                        <th>Sopir</th>
                        <th>Kendaraan</th>
                        <th>Penerima</th>
                        <th>Hormat Kami</th>
                        <th>Cek BK</th> --}}
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection


@push('js')
  <script type="text/javascript">
    console.log('form');

  
    // var datatableBk nama buat sendiri
    var datatableBk = $('#tbBk').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            // bk.datatable nama sesuai dengan controler
            url: '{{ route('banding.datatable') }}', 
        },
        columns: [
          // { data: 'DT_RowIndex', name: 'DT_RowIndex' }, untuk menampilkan no urut otomatis
             { data: 'DT_RowIndex', name: 'DT_RowIndex' },
             // { data: '.....', name: '.....' }, sesuai dengan select pada function datatable
             { data: 'tanggal', name: 'tanggal' },
             { data: 'no_bk', name: 'no_bk' },
            //  { data: 'jenis_bk', name: 'jenis_bk' },
            //  { data: 'kepada', name: 'kepada' },
            //  { data: 'sopir', name: 'sopir' },
            //  { data: 'nama', name: 'nama' },
            //  { data: 'penerima', name: 'penerima' },
            //  { data: 'hormat_kami', name: 'hormat_kami' },
            //  { data: 'is_cek_bk', name: 'is_cek_bk' },
             { data: 'opsi', name: 'opsi' }
        ]
    });

    $(document).on('click', '.pr', function () {
      var no_bk = $(this).attr('id');

      $.ajax({
        type : 'POST',
        url : '{{ route('get_no_banding.print') }}',
        data : {
                '_noBk' : no_bk
        },
        success :function (e) {
          // notif(e.code, e.msg);
          // console.log(e);
          notif(e.code, e.msg);
          datatableBk.ajax.reload();
          window.location.href = 'bk/print/'+no_bk;
        }
            
      });
    })

    // function delete_bk(.....) dan  data    : { '_noBk' : .... }, nama parameter harus sesuai
    function delete_bk(no_bk) {
      $.ajax({
        type    : 'POST',
        url     : '{{ route('bk_banding.delete') }}',
        // '_noBk' sesuai dengan function delete 
        data    : { '_noBk' : no_bk },
        success : function (e) {
          notif(e.code, e.msg);
          datatableBk.draw();          
        }
      });
    }

  </script>
@endpush 
