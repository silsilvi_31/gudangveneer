@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
      {{-- modal --}}
      <div class="modal fade bs-example-modal-lg" id="modal_brg" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel">Data Barang</h4>
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <table class="table table-striped table-bordered" id="tabel_brg" style="width:100%">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama</th>
                                <th>Harga</th>
                                {{-- <th>Satuan</th> --}}
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
      </div>
      {{-- //modal --}}
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Form Sampah</h2>
              <ul class="nav navbar-right panel_toolbox">
              
                <a href="{{route('bk_banding.index')}}" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left"></i></a>
                <a href="{{route('bk_banding.form')}}" class="btn btn-sm btn-success">New</a>
                </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
               {{-- kiri --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. BK
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                    <input type="hidden" id="formBk" name="" required="required" class="form-control" readonly value="{{!empty($form) ? $form : ''}}">
                      <input type="text" id="noBk" name="" required="required" class="form-control" readonly value="{{ !empty($no_bkQ) ? $no_bkQ : '' }}">
                    </div>
                  </div>

                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Tanggal
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="tanggalBk" name="" required="required" class="form-control" value="{{ !empty($tanggal) ? $tanggal : '' }}">
                    </div>
                  </div>

                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Kepada
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      {{-- <input type="text" id="kepadaBk" name="" required="required" class="form-control" value=""> --}}
                      <select id="kepadaBk" name="" class="form-control">
                      <option value="">-</option>
                      @php
                      $kepadaQ = !empty($id_kepada) ? $id_kepada : ''; 
                      $sopirQ = !empty($id_kar) ? $id_kar : '';
                      $jeniskendaraanQ = !empty($id_kendaraan) ? $id_kendaraan : '' ;
                      // jika variabel $jenis_bk tidak kosong maka muncul $jenis_bk jika kosong maka ''
                      $jenis_bkQ = !empty($jenis_bk) ? $jenis_bk : '';
                        
                      @endphp
                      @foreach ($kepadas as $itemq)
                        <option value="{{ $itemq->id }}" @if ($itemq->id == $kepadaQ) selected @endif>{{ $itemq->kepada }}</option>
                      @endforeach
                    </select>
                    </div>
                  </div>

                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Jenis BK
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      {{-- <input type="text" id="jenisBk" name="" required="required" class="form-control" value=""> --}}
                    <select id="jenisBk" name="" class="form-control">
                      {{-- <option value="">-</option> --}}
                      <option value="S" selected >S (Stok)</option>
                      {{-- <option value="P" @if($jenis_bkQ == 'P') selected @endif>P (Kembali)</option>
                      <option value="U" @if($jenis_bkQ == 'U') selected @endif>U (Uang)</option> --}}
                    </select>
                    
                    </div>
                  </div>

                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Sopir
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      {{-- <input type="text" id="sopirBk" name="" required="required" class="form-control" value=""> --}}
                      <Select id="sopirBk" name="" class="form-control">  
                        <option value="">-</option>
                        @foreach ($sopir as $item)
                          <option value="{{ $item->kode }}" @if($item->kode == $sopirQ) selected @endif>{{ $item->nama }}</option>
                        @endforeach
                      </Select>
                    </div>
                  </div>

                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Jenis Kendaraan
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      {{-- <input type="text" id="jeniskendaraanBk" name="" required="required" class="form-control" value=""> --}}
                      <select id="jeniskendaraanBk" name="" class="form-control">
                        <option value="">-</option>
                        {{-- $kendaraan ambil dari controller --}}
                        @foreach ($kendaraan as $item)
                        {{-- id ambil sesuai dengan tabel kendaraan --}}
                          <option value="{{ $item->id }}" @if ($item->id == $jeniskendaraanQ) selected @endif> {{ $item->nama }} </option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                {{-- kanan --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Penerima
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="penerimaBk" name="" required="required" class="form-control" value="{{ !empty($penerima) ? $penerima : '' }}">
                    </div>
                  </div>

                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Hormat Kami
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="hormatkamiBk" name="" required="required" class="form-control" readonly value="{{ !empty($hormat_kami) ? $hormat_kami : !empty($hormat) ? $hormat : '' }}">
                    </div>
                  </div>

                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Keterangan
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="keterangan" name="" required="required" class="form-control" value="{{ !empty($keterangan) ? $keterangan : '' }}">
                    </div>
                  </div>
                </div>

                {{-- u/garis --}}
                <div class="clearfix"></div>
                <div class="ln_solid"></div>

                {{-- kiri --}}
                <div class="col-md-6 col-sm-6">

                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Nama Barang
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="idQ" name="" class="form-control" value="" readonly>
                      <input type="text" id="idBarang" name="" class="form-control" value="" readonly>
                      <input type="text" id="namaBarang" name="" class="form-control" value="">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Harga
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="hargaBarang" name="" class="form-control" value="" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Qty
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="qty" name="" class="form-control" value="">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Satuan
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <select name="" id="idSatuan" class="form-control">
                        <option value="">-</option>
                        @foreach ($satuan as $item)
                          <option value="{{ $item->id }}">{{ $item->nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>

                {{-- kanan --}}
                {{-- <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Sub Total
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="subtotalBk" name="" required="required" class="form-control" readonly value="">
                    </div>
                  </div>
                </div> --}}
                {{-- u/garis --}}
                <div class="clearfix"></div>
                <div class="ln_solid"></div>

                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <div class="offset-md-3">
                      <button type="button" class="btn btn-primary">Cancel</button>
                      <button type="reset" class="btn btn-danger" >Reset</button>
                      <button type="button" class="btn btn-success" onclick="simpan_bk()" >Submit</button>

                    </div>
                  </div>
                </div>
              </form>
              {{-- u/garis --}}
              <div class="clearfix"></div>
              <div class="ln_solid"></div>
              <table class="table table-striped table-border" id="tbItemBk" style="width:100%">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Nama Barang</th>
                    <th>Banyak</th>
                    <th>Satuan</th>
                    <th>Harga</th>
                    <th>Sub Total</th>
                    <th>Opsi</th>
                  </tr>
                </thead>
              </table>
              
              <div class="clearfix"></div>
              <div class="ln_solid"></div>
              <div class="col-md-8">

              </div>
              <div class="col-md-4">
                <table class="table table-border" id="tbtotal" style="width:100%">
                  <thead>
                    <tr>
                      {{-- <th>No.</th>
                      <th>Nama Barang</th>
                      <th>Banyak</th>
                      <th>Satuan</th>
                      <th>Harga</th>
                      <th>Sub Total</th>
                      <th>Opsi</th> --}}
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td width='20%'><b>Total :</b></td>
                      <td>
                        <input type="text" id="total_harga" required="required" class="form-control" readonly value="">
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              

            </div>
          </div>
        </div>
      </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
    
    var form = $('#formBk').val();
    var nobk = $('#noBk').val();
    get_total();
    if (form == 'edit') {

    } else {
      max_no_bk()
    }

    function max_no_bk() {
      $.ajax({
        type : 'GET',
        url : '{{ route('bk_banding.max_no_bk') }}',
        success : function (e){
          console.log(e);
          $('#noBk').val(e);
        }
      });
    }
    
    var tb_item;
    
    // modal barang
    var tb_barang = $('#tabel_brg').DataTable({
      processing:true,
      serverSide: true,
      ajax:{
        type: 'GET',
        url: '{{ route('bk_brg.list') }}',
      },
      columns:[
        {data: 'DT_RowIndex', name: 'DT_RowIndex'},
        {data: 'nama_brg', name: 'nama_brg'},
        {data: 'harga', name: 'harga'},
        // {data: 'satuan', name: 'satuan'},
        {data: 'opsi', name: 'opsi'}
      ]
    }); 

    tb_item = $('#tbItemBk').DataTable({
      processing:true,
      serverSide: true,
      ajax:{
        type: 'POST',
        url: '{{ route('bk_banding.item') }}',
        data : function (e) {
          e._noBk = $('#noBk').val();
        },
      },
      columns:[
        {data: 'DT_RowIndex', name: 'DT_RowIndex'},
        {data: 'nama_brg', name: 'nama_brg'},
        {data: 'qty', name: 'qty'},
        {data: 'satuan', name: 'satuan'},
        {data: 'harga', name: 'harga'},
        {data: 'sub_total', name: 'sub_total'},
        {data: 'opsi', name: 'opsi'},
      ]
    }); 

    tb_item.draw();    

    $('#tanggalBk').datetimepicker({
      format: 'YYYY-MM-DD'
    });

    $('#tanggalBk').on("dp.change", function (e) {
        var tgl = $(this).val();
        console.log(tgl);    

        $.ajax({
          type : 'POST',
          url  : '{{route('get_no_bk_banding.json')}}',
          data : { '_tgl' : tgl },
          success : function (e) {
            console.log(e);
            $('#noBk').val(e);
          }
        });
    });


    $('#namaBarang').click(function () {
      $('#modal_brg').modal('show');
    });

    function select_barang(id_brg, nama, harga) {
      // console.log(id_brg+' '+nama);
      $('#idBarang').val(id_brg);
      $('#namaBarang').val(nama);
      $('#hargaBarang').val(harga);

      $('#modal_brg').modal('hide');
    }

    function simpan_bk() {
      var no_bk = $('#noBk').val();
      var tanggalBk = $('#tanggalBk').val();
      var kepadaBk = $('#kepadaBk').val();
      var jenisBk = $('#jenisBk').val();
      var sopirBk = $('#sopirBk').val();
      var jeniskendaraanBk = $('#jeniskendaraanBk').val();
      var penerimaBk = $('#penerimaBk').val();
      var hormatkamiBk = $('#hormatkamiBk').val();
      var keteranganBk = $('#keterangan').val();
      
      var idQ = $('#idQ').val();
      var idBarang = $('#idBarang').val();
      var idSatuan = $('#idSatuan').val();
      var harga = $('#hargaBarang').val();
      var qty = $('#qty').val();
    
      $.ajax({
        type : 'POST',
        url   : '{{route('bk_banding.save')}}',
        data : {
                '_noBk' : no_bk,
                '_tanggalBk' : tanggalBk,
                '_idKepadaBk' : kepadaBk,
                '_jenisBk' : jenisBk,
                '_sopirBk' : sopirBk,
                '_jeniskendaraanBk' : jeniskendaraanBk,
                '_penerimaBk' : penerimaBk,
                '_hormatkamiBk' : hormatkamiBk,
                '_keteranganBk' : keteranganBk,
                '_idQ' : idQ,
                '_idBarang' : idBarang,
                '_idSatuan' : idSatuan,
                '_hargaBarang' : harga,
                '_qty' : qty,
              },
        success : function (e){
          clear_barang();
          // console.log(e);
          notif(e.code, e.msg); 
          tb_item.draw();
          get_total();
        }
      });  
    }

    function clear_barang() {
      $('#idQ').val('');
      $('#idBarang').val('');
      $('#namaBarang').val('');
      $('#hargaBarang').val('');
      $('#qty').val('');
      $('#idSatuan').val('');
    }

    function delete_item(id_item) {
      // console.log(id_item);
      $.ajax({
        type : 'POST',
        url : '{{ route('bk_item.delete')}}',
        data: {
              '_id': id_item
        },
        success : function (e) {
          console.log(e);
          tb_item.draw();
          get_total();

        }
      })
    } 

    function get_total() {
      var no_bk = $('#noBk').val();
      $.ajax({
      type : 'POST',
      url : '{{route('total_harga.json')}}',
      data : {
              _noBk : no_bk 
             },
             success : function (e){
               console.log(e);
               $('#total_harga').val(e);               
             }
      }) 
    }


  </script>
@endpush