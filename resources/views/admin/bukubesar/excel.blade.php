{{-- @extends('master.print') --}}
    
{{-- @section('content') --}}
<style>
    td .total {
        background-color : #f39c12 !important;
    }
</style>
<h3>Rekap Neraca {{ date('d-m-Y', strtotime($data['tgl_m'])) }} - {{ date('d-m-Y', strtotime($data['tgl_a'])) }}</h3>
<br>

<div class="row">
    <div class="col-md-12">
        <div class="col-md-6">
            <table>
                <tr>
                    <th>Akun/Tgl</th>
                    <th>Transaksi</th>
                    <th>Keterangan</th>
                    <th>Ref</th>
                    <th>Debit</th>
                    <th>Kredit</th>
                    {{-- <th>Saldo</th> --}}
                </tr>
                @foreach ($data['rekap'] as $value)
                    <tr>
                        <td>{{ $value->nama_akun }}</td>
                        <td>{{ $value->jenis_jurnal }}</td>
                        <td>{{ $value->keterangan }}</td>
                        <td>{{ $value->ref}}</td>
                        <td>{{ $value->debit}}</td>
                        <td>{{ $value->kredit}}</td>
                    </tr>
                @endforeach
            </table>
        </div>

    </div>
</div>

{{-- @endsection --}}