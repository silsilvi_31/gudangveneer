@extends('master.ella')
    
@section('content')
<style type="text/css">
  body {
    /* border: 1px solid #000 !important; */
  }
  .table-bordered, .table th {
    /* border-bottom: 1px solid #000 !important; */
    border: none !important;
    padding: .3rem;
  }
  .x_panel {
    border : none !important;
  }
  .table td {
    border-bottom: none !important;
    padding: .3rem;
  }
  .nominal {
    float: right;
  }
  tr .total_gf {
    border: 1px solid #000 !important;
  }

  .no {
    width : 10%;
  }

  .akun {
    width: 45%;
  }

  .angka {
    width: 15%; 
  }
</style>

<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Laba Rugi</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <label class="col-form-label col-md-4 label-align" for="first-name">Tanggal</label>
                <div class="col-md-2">
                  <input type="text" id="Tgl" name="tgl" class="form-control" autocomplete="off">
                </div>
                <div class="col-md-2">
                  <input type="text" id="tglDua" name="tgl_dua" class="form-control" autocomplete="off">
                </div>
                <button type="button" class="btn btn-info" onclick="cari_labarugi()">Cari</button>
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                  <div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle"
                      data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Export
                    </button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                      <button class="dropdown-item" onclick="excel_labarugi()">Excel</button>
                      <button class="dropdown-item" onclick="pdf_labarugi()">Pdf</button>
                      <a class="dropdown-item" href="#">Csv</a>
                      <a class="dropdown-item" href="#">Print</a>
                    </div>
                  </div>
                </div>

              <div class="col-md-12">
                <div class="card-box table-responsive">
                  Pendapatan Laba Rugi
                  <table id="tb_pendapatan" class="table table-striped jambo_table bulk_action" style="width:100%">
                    <thead>
                      <tr>
                        <th class="no">No Akun</th>
                        <th class="akun">Keterangan</th>
                        {{-- <th class="angka text-right">Debit</th> --}}
                        <th class="angka text-right"></th>
                        <th class="angka text-right">Total</th>
                      </tr>
                    </thead>
                    <tbody id="body_pendapatan">
                      
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="2" id="label_pend"></td> 
                        {{-- <td class="text-right" id="total_debit_pend"></td> --}}
                        <td class="text-right" id="total_kredit_pend"></td>   
                        <td class="text-right" id="total_pend" style="background:#8395a7;"></td>  
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
  
  var tt_pendapatan;
    var tt_hpp;
    var tt_beban;
    var hasil;

    $('#Tgl').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    $('#tglDua').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    // $("#Tgl").on("dp.change", function (e) {
    //     var tgl = $(this).val();
    //     var tgl_dua = $('#tglDua').val();
    //     tampil_pendapatan(tgl, tgl_dua);
    //     tampil_hpp(tgl, tgl_dua);
    //     tampil_beban(tgl, tgl_dua);
    //     labarugi();
    // });

    // $("#tglDua").on("dp.change", function (e) {
    //     var tgl = $('#Tgl').val();
    //     var tgl_dua = $(this).val();
    //     tampil_pendapatan(tgl, tgl_dua);
    //     tampil_hpp(tgl, tgl_dua);
    //     tampil_beban(tgl, tgl_dua);
    //     labarugi();
    // });

    function cari_labarugi() {
      var tgl = $('#Tgl').val();
      var tgl_dua = $('#tglDua').val();

      tampil_pendapatan(tgl, tgl_dua);

    }

    tampil_pendapatan();

    function tampil_pendapatan(tgl, tgl_dua) {
      tr = '';
      $ketr = '';
      $.ajax({
        type    : 'POST',
        url     : '{{ route('labarugi.datatable_pendapatan') }}',
        data    : {
                    '_tgl'    : tgl,
                    '_tglDua' : tgl_dua
                },
        success : function (e) {
          console.log(e);
          $('#body_pendapatan').empty();
          $.each(e.data, function (key, value) {
            if (value.tipe == 'parent') {
              tr += '<tr>';
              tr += '<td><strong>'+value.no_akun+'</strong></td>';
              tr += '<td><strong>'+value.akun+'</strong></td>';
              // tr += '<td class="text-right">'+value.debit+'</td>';
              tr += '<td class="text-right"></td>';
              tr += '<td class="text-right">'+value.format_total+'</td>';
              tr += '</tr>'; 
            } else if (value.tipe == 'child') {
              tr += '<tr>';
              tr += '<td>&nbsp&nbsp'+value.no_akun+'</td>';
              tr += '<td>'+value.akun+'</td>';
              // tr += '<td class="text-right">'+value.debit+'</td>';
              tr += '<td class="text-right"></td>';
              tr += '<td class="text-right">'+value.format_total+'</td>';
              tr += '</tr>'; 
            } else {
              tr += '<tr>';
              tr += '<td>&nbsp&nbsp'+value.no_akun+'</td>';
              tr += '<td>'+value.akun+'</td>';
              // tr += '<td class="text-right">'+value.debit+'</td>';
              tr += '<td class="text-right"><strong>Total</strong></td>';
              tr += '<td class="text-right">'+value.total+'</td>';
              tr += '</tr>'; 
            }
          });

          if (e.total_all < 0) {
            ketr = 'Laba'
          } else if (e.total_all > 0) {
            ketr = 'Rugi'
          } else {
            ketr = '-'
          }

          $('#label_pend').text(ketr);
          $('#total_pend').text(e.format_total_all);
          $('#body_pendapatan').append(tr);   
        }
      });
    }

    function numberformat(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    } 

    function excel_labarugi() {
      var tgl_m = $('#Tgl').val();
      var tgl_a = $('#tglDua').val();

      var tgl_gabung = tgl_m+'&'+tgl_a;

      var url = '{{ route('labarugi.excel_labarugi', ['.p']) }}';
      var url_fix = url.replaceAll('.p',tgl_gabung);
      // console.log(url_fix);
      window.location.href = url_fix;
    }

    function pdf_labarugi() {
      var tgl_m = $('#Tgl').val();
      var tgl_a = $('#tglDua').val();

      var tgl_gabung = tgl_m+'&'+tgl_a;

      var url = '{{ route('labarugi.pdf_labarugi', ['.p']) }}';
      var url_fix = url.replaceAll('.p',tgl_gabung);
      // console.log(url_fix);
      window.location.href = url_fix;
    }
  </script>
@endpush