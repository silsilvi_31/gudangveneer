@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>

{{-- modal kepada --}}
<div class="modal fade bs-example-modal-lg" id="modal_kepada" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Data Kepada</h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="kepadamd" name="kepada_md" required="required" class="form-control">
                </div>
              </div>
              <div class="clearfix"></div>
          <div class="ln_solid"></div>
          
            <div class="form-group">
              <div class="offset-md-3">
                {{-- <button type="button" class="btn btn-sm btn-danger">Cancel</button> --}}
                <button class="btn btn-sm btn-danger" type="reset">Reset</button>
                <button type="button" class="btn btn-sm btn-success" onclick="add_kepada()">Submit</button>
              </div>
            </div>
            </div>
      </div>
  </div>
</div>
{{-- //modal --}}



      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Form Daftar Harga Kayu Bakar</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form id="" action="{{ route('hargakayubakar.save') }}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                {{ csrf_field() }}
                {{-- <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Pelanggan <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="" name="pelanggan" required="required" class="form-control " autocomplete="off">
                  </div>
                </div> --}}
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Pelanggan <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <select name="pelanggan" id="pelanggan" class="form-control ">
                      <option name='pelanggan_kosong' id="pelanggan_kosong" value="">-</option>
                      @php
                      $kepadaQ = !empty($kepada) ? $kepada: '';
                      $barangQ = !empty($barang) ? $barang: '';
                      @endphp
                     
                      @foreach ($kepada as $item)
                        <option value="{{ $item->id }}">{{ $item->kepada }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-md-1">
                    <button type="button" class="btn btn-info kepadaQ"><i class="fa fa-plus"></i></button>
                  </div>
                </div>
                {{-- <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> No. Akun Pelanggan <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="" name="akunpelanggan" required="required" class="form-control " autocomplete="off">
                  </div>
                </div> --}}
                {{-- <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Barang <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <select name="barang" id="" class="form-control ">
                      <option value="">-</option>
                      @foreach ($barang as $item)
                        <option value="{{ $item->kode }}">{{ $item->nama_brg }}</option>
                      @endforeach
                    </select>
                  </div>
                </div> --}}
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Barang <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="barang" name="barang" required="required" class="form-control " autocomplete="off">
                    ex : kayu bakar sabetan triplek @truck
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Satuan <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <select name="id_satuan" id="id_satuan" class="form-control ">
                      <option value="">-</option>
                      @foreach ($satuan as $item)
                        <option value="{{ $item->id }}">{{ $item->nama }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Harga <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="harga" name="harga" required="required" class="form-control " autocomplete="off">
                  </div>
                </div>
                
                <div class="ln_solid"></div>
                <div class="item form-group">
                  <div class="col-md-6 col-sm-6 offset-md-3">
                    <a href="{{ route('hargakayubakar.index') }}" class="btn btn-warning" type="button">Cancel</a>
                    <button class="btn btn-danger" type="reset">Reset</button>
                    <button type="submit" class="btn btn-success" onclick="simpan_bk()">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
    console.log('form');
    // var datatablePresensi = $('#tbJabatan').DataTable({
    //     processing: true,
    //     serverSide: true,
    //     ajax: {
    //         type: 'GET',
    //         url: '{{ route('jabatan.datatable') }}', 
    //     },
    //     columns: [
    //       { data: 'DT_RowIndex', name: 'DT_RowIndex' },
    //       { data: 'jabatan', name: 'jabatan' },
    //       { data: 'opsi', name: 'opsi' }
    //     ]
    // });

    $(document).on('click', '.kepadaQ', function(){  
      $('#modal_kepada').modal('show');  
    });

    function clear_kepada(){
      $('#kepadamd').val('');
    }

    function simpan_bk() {
      // var idQ = $('#idQ').val();
      var pelanggan = $('#pelanggan').val();
      var barang = $('#barang').val();
      var id_satuan = $('#id_satuan').val();
      var harga = $('#harga').val();
    
      $.ajax({
        type : 'POST',
        url   : '{{route('hargakayubakar.save')}}',
        data : {
                // '_idQ' : idQ,
                '_pelanggan' : pelanggan,
                '_barang' : barang,
                '_id_satuan' : id_satuan,
                '_harga' : harga,
              },
        success : function (e){
          clear_barang();
          // console.log(e);
          notif(e.code, e.msg); 
          // tb_item.draw();
          // get_total();
        }
      });  
    }

    function clear_barang() {
      // $('#idQ').val('');
      $('#pelanggan').val('');
      $('#barang').val('');
      $('#id_satuan').val('');
      $('#harga').val('');
    }

    function add_kepada() {
      var kepada_md = $('#kepadamd').val();

      $.ajax({
        type : 'POST',
        url : '{{ route('hargakayubakar.add_kepada') }}',
        data : {
                '_kepadamd' : kepada_md,
              },
        success: function (e) {
          clear_kepada();
          console.log(e);
          notif(e.code, e.msg);
          $('#modal_kepada').modal('hide');
          html = '<option value='+e.id+'>'+e.kepada_md+'</option>';
          $('#pelanggan').prepend(html);
          $("#pelanggan option:first-child").attr("selected", true);
          $('#pelanggan_kosong').val();
        }
      });
    }

  </script>
@endpush