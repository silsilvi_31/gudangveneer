@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

            {{-- Awal Modal Klaim Absen --}}
            <div id="modal_claim" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
        
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Detail Absensi</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                    {{-- Kiri --}}
                    <div class="col-md-6 col-sm-6">
                      <div class="item form-group">
                        <label class="col-form-label col-md-5 col-sm-5 label-align" for="first-name">Tgl <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="Tgl" name="tgl" class="form-control" readonly>
                        </div>
                      </div>
                    </div>
                    </form>
                    <div class="card-box table-responsive">
                      <table id="tbDetailAbsen" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                          <tr>
                            <th></th>
                            <th>No.</th>
                            <th>Tgl</th>
                            <th>Kodep</th>
                            <th>Jam Hadir</th>
                            <th>Jam Pulang</th>
                          </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="claim_absen()">Claim</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
        
                </div>
              </div>
            </div>
            {{-- Akhir Modal Klaim Absen --}}

      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Gaji</h2>
            {{-- <ul class="nav navbar-right panel_toolbox">
              <a href="{{ route('claimabsen.form') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a>
            </ul> --}}
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <label class="col-form-label col-md-3 label-align" for="first-name">Tanggal</label>
                <div class="col-md-2">
                  <input type="text" id="tglAwal" name="tgl_awal" class="form-control" autocomplete="off">
                </div>
                {{-- <div class="col-md-2">
                  <input type="text" id="tglAkhir" name="tgl_akhir" class="form-control" autocomplete="off">
                </div> --}}
                  <button type="button" onclick="gaji_mingguan()" class="btn btn-primary">Gaji Mingguan</button>
                </div>
                  <table id="tb_gaji" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tgl</th>
                        <th>Total</th>
                        <th>Jumlah Karyawan</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">  
    $('#tglAwal').datetimepicker({
      format: 'DD-MM-YYYY'
    });  
    $('#tglAkhir').datetimepicker({
      format: 'DD-MM-YYYY'
    }); 

    var tb_gaji = '';

    tb_gaji = $('#tb_gaji').DataTable({
      processing    : true,
      serverSide    : true,
      ajax  : {
          type : 'GET',
          url : '{{ route('gaji.datatable') }}',

      },
      columns: [
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'tgl', name: 'tgl' },
        { data: 'total', name:'total' },
        { data: 'total_kar', name:'total_kar' },
        { data: 'opsi', name: 'opsi' }
      ]
    });

    function delete_claim(tgl) {
      var txt;
      var r =  confirm('Apakah Anda yakin ingin menghapus Gaji?');

      if (r == true) {
        $.ajax({
            type : 'POST',
            url : '{{ route('gaji.delete') }}',
            data : { '_tgl' : tgl },
            success : function (e) {
              notif(e.response.code, e.response.msg);
              tb_gaji.draw();
            }
        });
      } else {
        txt = 'n';
      } 
    }

    function gaji_mingguan() {
      var tgl_awal = $('#tglAwal').val();
      // var tgl_akhir = $('#tglAkhir').val();

      // var tgl_gabung = tgl_awal+'&'+tgl_akhir;
      var url = '{{ route('gaji.gaji_mingguan', ['.p']) }}';
      var url_fix = url.replaceAll('.p',tgl_awal);

      window.location.href = url_fix;
    }
  </script>
@endpush