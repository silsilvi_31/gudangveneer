@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    <!-- ============================================================== -->
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Form Absen</h2>
              <ul class="nav navbar-right panel_toolbox">
              </ul>
              <div class="clearfix"></div>
            </div>

            <div class="x_content">
              <br/>
              <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                {{-- {{ csrf_field() }} --}}
                {{-- Kiri --}}
                <div class="col-md-12 col-sm-12">
                  <div class="item form-group"> 
                    <label class="col-form-label col-md-3 col-sm-3" for="first-name">Tanggal
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Tgl" name="tgl" required='required' class="form-control" value="{{ !empty($tgl) ? $tgl : '' }}">
                    </div>
                  </div>
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="tb_gaji" style="width:100%">
                      <thead>
                        <tr>
                          <th>Nama</th>
                          <th>Kodep</th>
                          <th>Gaji</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody id="bd_absen">
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="clearfix"></div>
                <div class="ln_solid"></div>

                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <div class="offset-md-3">
                      {{-- <button type="button" class="btn btn-primary">Cancel</button> --}}
                      {{-- <button class="btn btn-sm btn-primary" type="reset">Reset</button>
                      <button type="button" class="btn btn-sm btn-success" onclick="save_jurnal()">Submit</button> --}}
                    </div>
                  </div>
                </div>
              </div>                                   
              </form>
              </div>
          </div>
        </div>
      </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
    $('#Tgl').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    var tb_gaji = '';

    tb_gaji = $('#tb_gaji').DataTable({
      processing    : true,
      serverSide    : true,
      ajax  : {
          type : 'GET',
          url : '{{ route('gaji.datatable_detail_gaji') }}',

      },
      columns: [
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'tgl', name: 'tgl' },
        { data: 'total', name:'total' },
        { data: 'opsi', name: 'opsi' }
      ]
    });

  </script>
@endpush