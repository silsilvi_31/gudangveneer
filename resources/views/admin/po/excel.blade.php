{{-- @extends('master.print')
    
@section('content') --}}
<h3>Rekap {{ date('d-m-Y', strtotime($data['tgl_m'])) }} - {{ date('d-m-Y', strtotime($data['tgl_a'])) }}</h3>
<br>
<table style="border:1px solid black;">
        <tr>
            <th>Tgl</th>
            <th>No Sj</th>
            <th>Nama Pelanggan</th>
            <th>Nama Brg</th>
            <th>Harga</th>
            <th>Banyak</th>
            <th>Subtotal</th>
        </tr>
        @foreach ($data['rekap'] as $value)
            <tr>
                <td>{{ date('d-m-Y', strtotime($value->tgl)) }}</td>
                <td>{{ $value->id }}</td>
                <td>{{ $value->nama_pelanggan }}</td>
                <td>{{ $value->nama_brg}}</td>
                <td>{{ $value->harga}}</td>
                <td>{{ $value->qty}}</td>
                <td>{{ $value->subtotal }}</td>
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><strong>Total</strong></td>
            <td>{{ $data['total'] }}</td>
        </tr>
    </table>
{{-- @endsection --}}