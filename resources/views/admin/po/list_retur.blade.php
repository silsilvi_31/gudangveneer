@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    <!-- ============================================================== -->
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Retur Penjualan</h2>
              <div class="clearfix"></div>
            </div>

            <div class="x_content">
              <table class="table table-sm table-striped table-bordered" id="tb_retur" style="width:100%">
                <thead>
                    <tr>
                      <th>No.</th>
                      <th>ID SJ</th>
                      <th>Tgl.</th>
                      {{-- <th>Nama</th> --}}
                      <th>Barang</th>
                      {{-- <th>Total</th> --}}
                    </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>                                   

            <div class="clearfix"></div>
            <div class="ln_solid"></div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
    var tb_retur = $('#tb_retur').DataTable({
        processing: true,
        serverSide: true,
        order: false,
        ajax: {
            type: 'GET',
            url: '{{ route('retur.datatable_retur') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'id_sj', name: 'id_sj' },
          { data: 'tgl_retur', name: 'tgl_retur' },
          // { data: 'nama', name: 'nama' },
          { data: 'nama_brg', name: 'nama_brg' },
          // { data: 'total', name: 'total' }
        ]
    });
  </script>
@endpush