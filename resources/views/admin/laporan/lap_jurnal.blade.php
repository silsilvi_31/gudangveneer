@extends('master.print')
    
@section('content')
<style>
  .table td, .table th {
    padding: 0px !important;
  }
</style>

<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

        <!-- page content -->
            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">

                    <section class="content invoice">
                       <!-- title row -->
                       <div class="row">
                         <div class="col-md-3"></div>
                          <div class="col-md-1">
                           <strong>Tgl.</strong> 
                          </div>
                          <div class="col-md-2">
                            <input type="text" id="tglAwal" name="tgl_awal" class="form-control">
                          </div>
                          <div class="col-md-2">
                            <input type="text" id="tglAkhir" name="tgl_akhir" class="form-control">
                          </div>
                          <div class="col-md-2">
                            <button class="btn btn-success">Excel</button>
                            <button class="btn btn-info">PDF</button>
                          </div>
                        </div> 
                      <!-- /.row -->

                      <br>
                      <!-- Table row -->
                      
                      <center><h1>Laporan Jurnal</h1></center>
                      <div class="row">
                          <table class="table table-striped jambo_table bulk_action" id="tb_nota">
                            <thead>
                              <tr id="header_nota">
                                <th>Tgl</th>
                                <th>Jurnal</th>
                                <th>No. Akun</th>
                                <th>Ref</th>
                                <th>Keterangan</th>
                                <th>Map</th>
                                <th>Hit</th>
                                <th>Qty</th>
                                <th>m3</th>
                                <th>Harga</th>
                                <th>Total</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach ($jurnal_sj as $item)
                                <tr>
                                  <td> {{ $item->tgl }} </td>
                                  <td> {{ $item->jurnal }} </td>
                                  <td> {{ $item->no_akun }} </td>
                                  <td> {{ $item->jenis_jurnal }} {{ $item->ref }} </td>
                                  <td> {{ $item->keterangan }} </td>
                                  <td> {{ $item->map }} </td>
                                  <td> {{ $item->qty }} </td>
                                  <td> {{ $item->hit }} </td>
                                  <td> {{ $item->m3 }} </td>
                                  <td> {{ $item->harga }} </td>
                                  <td> {{ $item->total }} </td>
                                </tr>
                                @endforeach
                                {{-- <tr>
                                  <td colspan="15" style="background:#f7d794;">Gaji</td>
                                </tr>  
                                  @foreach ($jurnal_gj as $item)
                                  <tr>
                                    <td> {{ $item->tgl }} </td>
                                    <td> {{ $item->jurnal }} </td>
                                    <td> {{ $item->no_akun }} </td>
                                    <td> {{ $item->jenis_jurnal }} {{ $item->ref }} </td>
                                    <td> {{ $item->keterangan }} </td>
                                    <td> {{ $item->map }} </td>
                                    <td> {{ $item->qty }} </td>
                                    <td> {{ $item->hit }} </td>
                                    <td> {{ $item->m3 }} </td>
                                    <td> {{ $item->harga }} </td>
                                    <td> {{ $item->total }} </td>
                                  </tr>
                                  @endforeach --}}
                            </tbody>
                          </table>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                  </div>
                </div>
              </div>
            </div>
        <!-- /page content -->

@endsection

@push('js')
  <script type="text/javascript">
    $(function(){
      // window.print();
    }); 
  </script>
@endpush