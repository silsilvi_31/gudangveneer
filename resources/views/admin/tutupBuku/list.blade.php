@extends('master.ella')
    
@section('content')
<style type="text/css">
  body {
    /* border: 1px solid #000 !important; */
  }
  .table-bordered, .table th {
    /* border-bottom: 1px solid #000 !important; */
    border: none !important;
    padding: .3rem;
  }
  .x_panel {
    border : none !important;
  }
  .table td {
    border-bottom: none !important;
    padding: .3rem;
  }
  .nominal {
    float: right;
  }
  tr .total_gf {
    border: 1px solid #000 !important;
  }

  .no {
    width : 10%;
  }

  .akun {
    width: 45%;
  }

  .angka {
    width: 15%; 
  }
</style>

<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Neraca</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <label class="col-form-label col-md-4 label-align" for="first-name">Tanggal</label>
                <div class="col-md-2">
                  <input type="text" id="Tgl" name="tgl" class="form-control" autocomplete="off">
                </div>
                <div class="col-md-2">
                  <input type="text" id="tglDua" name="tgl_dua" class="form-control" autocomplete="off">
                </div>

              <div class="card-box table-responsive">
                <table id="tabel_tutup_buku" class="table table-striped jambo_table bulk_action" style="width:100%">
                  <thead>
                    <tr>
                      <th class="no">No Akun</th>
                      <th class="akun">Keterangan</th>
                      <th class="angka text-right">Debit</th>
                      <th class="angka text-right">Kredit</th>
                      <th class="angka text-right">Total</th>
                    </tr>
                  </thead>
                  <tbody id="body_aktiva">
                    
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="2" ><strong>Total</strong></td> 
                      <td class="text-right" id="tt_d_tutupbuku"></td>
                      <td class="text-right" id="tt_k_tutupbuku"></td>   
                      <td class="text-right" id="tt_all" style="background:#8395a7;"></td>  
                    </tr>
                  </tfoot>
                </table>
              </div>
            
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
     var tabel_tutup_buku = $('#tabel_tutup_buku').DataTable({
        processing: true,
        serverSide: true,
        order: false,
        ajax: {
            type: 'POST',
            url: '{{ route('tutupBuku.datatable') }}', 
        },
        columns: [
          { data: 'no_akun', name: 'no_akun' },
          { data: 'keterangan', name: 'keterangan' },
          { data: 'debit', name: 'debit' },
          { data: 'kredit', name: 'kredit' },
          { data: 'total', name: 'total' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

  </script>
@endpush