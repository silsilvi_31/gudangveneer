@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel"></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="form-horizontal form-label-left">
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kode
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="kode" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="nama" class="form-control ">
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Jabatan
                </label>
                <div class="col-md-6 col-sm-6 ">
                  {{-- <input type="text" id="" name="id_jabatan" required="required" class="form-control"> --}}
                  <select name="id_jabatan" id="" class="form-control">
                    @php
                        $data = DB::table('jabatan')
                                ->where('status', NULL)
                                ->get();
                    @endphp
                    <option value=""> - </option>
                    @foreach ($data as $item)
                        <option value="{{ $item->id }}"> {{ $item->jabatan }} </option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Alamat
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="alamat" class="form-control ">
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Telp
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="no_telp" class="form-control ">
                </div>
              </div>
                            
              <div class="ln_solid"></div>
              <div class="item form-group">
                <div class="col-md-6 col-sm-6 offset-md-3">
                  <button type="button" class="btn btn-success btn-kry" onclick="update_karyawan()"></button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Karyawan</h2>
            <ul class="nav navbar-right panel_toolbox">
              <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-edit" data-form="add"><i class="fa fa-plus"></i></button>
              {{-- <a href=" {{ route('akun.form') }} " class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a> --}}
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="card-box table-responsive">
                  <table id="tbKaryawan" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Jabatan</th>
                        <th>Alamat</th>
                        <th>No. Telp</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
    // console.log('uyeee');
    var datatableKaryawan = $('#tbKaryawan').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '{{ route('karyawan.datatable') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'kode', name: 'kode' },
          { data: 'nama', name: 'nama' },
          { data: 'jabatan', name: 'jabatan' },
          { data: 'alamat', name: 'alamat' },
          { data: 'no_telp', name: 'no_telp' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

    $('#modal-edit').on('shown.bs.modal', function (event) {
      var $button = $(event.relatedTarget);
      var form = $button.data('form'); 
      var kode = $button.data('kode');
      var nama = $button.data('nama');
      var id_jabatan = $button.data('id_jabatan');
      var alamat = $button.data('alamat');
      var no_telp = $button.data('no_telp');

      console.log(form);
      if (form == 'edit') {
        $('#myModalLabel').html('Edit Karyawan');
        $('.btn-kry').html('Update');
        $('input[name=kode]').val(kode);
        $('input[name=nama]').val(nama);
        $('select[name=id_jabatan]').val(id_jabatan);
        $('input[name=kode]').attr('readonly', true);
        $('input[name=alamat]').val(alamat);
        $('input[name=no_telp]').val(no_telp);  
      }else if(form == 'add'){
        $('#myModalLabel').html('Form Karyawan');
        $('.btn-kry').html('Save');
        $('input[name=kode]').val('');
        $('input[name=kode]').attr('readonly', false);
        $('input[name=nama]').val('');
        $('select[name=id_jabatan]').val('');
        $('input[name=alamat]').val('');
        $('input[name=no_telp]').val(''); 
      }

    });

    function update_karyawan() {
      var kode = $('input[name=kode]').val();
      var nama = $('input[name=nama]').val();
      var id_jabatan = $('select[name=id_jabatan]').val();
      var alamat = $('input[name=alamat]').val();
      var no_telp = $('input[name=no_telp]').val();

      if (!kode) {
        alert('Kode tidak boleh kosong !');
      }else if (!nama) {
        alert('Nama tidak boleh kosong !');
      }else{
        $.ajax({
          type  : 'POST',
          url   : '{{ route('karyawan.save') }}',
          data  : {
                    '_kode' : kode,
                    '_nama' : nama,
                    '_idJabatan' : id_jabatan,
                    '_alamat' : alamat,
                    '_noTelp' : no_telp
                  },
          success : function (e) {
            datatableKaryawan.draw();
            $('#modal-edit').modal('hide');
            notif(e.code, e.msg);
          }
        });
      }
    }

    function delete_karyawan(kode) {
      $.ajax({
        type    : 'POST',
        url     : '{{ route('karyawan.delete') }}',
        data    : { '_kode' : kode },
        success : function (e) {
          notif(e.code, e.msg);
          datatableKaryawan.draw();
        }
      });
    }

  </script>
@endpush