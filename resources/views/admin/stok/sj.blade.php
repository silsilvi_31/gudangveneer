@extends('master.print')
    
@section('content')
<style type="text/css">
   body {
     /* border: 1px solid #000 !Important; */
    font-size : 1.3em !important;
    padding: .3rem;
  }
  .table-bordered, .table th {
    border: 1px solid #000 !important;
    padding: .3rem;
  }
  .x_panel {
    border : none !important;
  }
  .table td {
    border: none !important;
    padding: .3rem;
  }
  .nominal {
    float: right;
  }

  </style>
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

        <!-- page content -->
            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">

                    <section class="content invoice">
                      <!-- title row -->
                      <center><h1>Surat Jalan</h1></center>
                       <!-- title row -->
                        <div class="row">
                          <div class="col-md-2">
                           <strong>No.</strong> 
                          </div>
                          <div class="col-md-6">
                             {{ $id }}
                          </div>
                          <div class="col-md-2">
                           <strong>Pengiriman</strong> 
                          </div>
                          <div class="col-md-2">
                            {{ $pengiriman }}
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-2">
                           <strong>Tanggal.</strong> 
                          </div>
                          <div class="col-md-6">
                             {{ $tgl }}
                          </div>
                          <div class="col-md-2">
                          <strong>Sopir</strong> 
                          </div>
                          <div class="col-md-2">
                            {{ $sopir }}
                          </div>
                        </div>
                      
                      <div class="clearfix"></div>
                      <div class="ln_solid" style="border: solid 1px #000 !important"></div>
                      
                      <div class="row">
                        <div class="col-md-2">
                         <strong>Kepada</strong> 
                        </div>
                        <div class="col-md-6">
                           {{ $nama }}
                        </div>
                        <div class="col-md-2">
                         <strong>Mobil</strong> 
                        </div>
                        <div class="col-md-2">
                          {{ $kendaraan }}
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-2">
                         {{-- <strong>Tanggal.</strong>  --}}
                        </div>
                        <div class="col-md-6">
                           {{ $alamat }}
                        </div>
                        <div class="col-md-2">
                         <strong>No. Plat</strong> 
                        </div>
                        <div class="col-md-2">
                          {{ $no_plat }}
                        </div>
                        <div class="col-md-2">
                          {{-- <strong>Tanggal.</strong>  --}}
                         </div>
                         <div class="col-md-6">
                            {{-- {{ $alamat }} --}}
                         </div>
                         <div class="col-md-2">
                          <strong>Catatan</strong> 
                         </div>
                         <div class="col-md-2">
                           {{ isset($catatan) ? $catatan : '-' }}
                         </div>
                      </div>
                      
                      <!-- /.row -->
                      <br>
                      <!-- Table row -->
                      <div class="row">
                        <div class="  table">
                          <table class="table" id="tb_nota">
                            <thead>
                              <tr>
                                <th><center>No.</center></th>
                                <th>Nama Barang</th>
                                <th>Satuan</th>
                                <th>Qty</th>
                              </tr>
                            </thead>
                            <tbody>
                              @php
                                  $no = 1;
                              @endphp
                                @foreach ($detail_barang as $item)
                                    <tr>
                                      <td> <center> {{ $no++ }} </center></td>
                                      <td> {{ $item->nama_brg }} </td>
                                      <td> {{ $item->satuan }} </td>
                                      <td> {{ $item->qty }} </td>
                                    </tr>
                                @endforeach
                                <tr style="border-top:1px solid #000;">
                                  <td colspan="3"><b style="float:right">Total</b></td>
                                  <td id="grandtotal" colspan="4">{{ number_format($jumlah, 0,',','.') }}</td>
                                </tr>
                            </tbody>
                          </table>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-md-3">
                          <p class="lead">Penerima</p>
                          
                        </div>
                        <div class="col-md-3">
                          <p class="lead">Sopir</p>
                          <br>
                          <br>
                          <p>{{ $sopir }}
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3">
                          <p class="lead"> Cek</p>
                          <br>
                          <br>
                          <p></strong></strong></p>
                        </div>

                        <div class="col-md-3">
                          <p class="lead"> Hormat Kami,</p>
                          <br>
                          <br>
                          <p></strong>{{ $mengetahui }}</strong></p>
                          </div>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- this row will not appear when printing -->
                      {{-- <div class="row no-print">
                        <div class=" ">
                          <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                          <button class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment</button>
                          <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
                        </div>
                      </div> --}}
                    </section>
                  </div>
                </div>
              </div>
            </div>
        <!-- /page content -->

@endsection

@push('js')
  <script type="text/javascript">
    $(function(){
      window.print();
    }); 
  </script>
@endpush