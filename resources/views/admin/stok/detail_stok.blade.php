@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    {{-- Modal Detail Stok --}}
    <div id="modal_detail_stok" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Detail Stok Masuk</h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
            {{-- Kiri --}}
            <div class="col-md-6 col-sm-6">
              <div class="item form-group">
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="Tgl" name="tgl" class="form-control" readonly>
                  <input type="text" id="namaBrg" name="nama_brg" class="form-control" readonly>
                  <input type="text" id="Jenis" name="jenis" class="form-control" readonly>
                </div>
              </div>
            </div>

            
            </form>
            <div class="clearfix"></div>
            <div class="ln_solid"></div>
            <table class="table table-striped table-bordered" id="tb_detail_stok_masuk" style="width:100%">
              <thead>
                  <tr>
                      <th>No.</th>
                      <th>Nama</th>
                      <th>Ketr</th>
                      <th>Satuan</th>
                      <th>Qty</th>
                      <th>Harga</th>
                      <th>Potongan</th>
                      <th>Subtotal</th>
                      <th>Opsi</th>
                  </tr>
              </thead>
              <tbody>
              </tbody>
          </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Approve</button>
          </div>

        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>{{ $nama_brg }}</h2>
            <ul class="nav navbar-right panel_toolbox">
              {{-- <a href="{{ route('sjdefault.form') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a> --}}
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <input type="hidden" id="kdBrg" value="{{ $kode }}">
                <div class="card-box table-responsive">
                  <table id="detail_stok_masuk" class="table table-sm table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No. Nota</th>
                        <th>Tanggal</th>
                        <th>Stok Awal</th>
                        <th>Stok Masuk</th>
                        <th>Stok Keluar</th>
                        <th>Sisa</th>
                        {{-- <th>Opsi</th> --}}
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
      $('#tglExport_m').datetimepicker({
        format: 'YYYY-MM-DD'
      });

      $('#tglExport_a').datetimepicker({
        format: 'YYYY-MM-DD'
      });
    // console.log('uyeee');
    var datatableSj = $('#detail_stok_masuk').DataTable({
        processing: true,
        serverSide: true,
        order: false,
        ajax: {
            type: 'POST',
            url: '{{ route('stok.datatable_detail') }}', 
            data: function (e) {
                  e._kodeBrg = $('#kdBrg').val();
            }
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'tgl', name: 'tgl' },
          { data: 'stok_awal', name: 'stok_awal' },
          { data: 'stok_masuk', name: 'stok_masuk' },
          { data: 'stok_keluar', name: 'stok_keluar' },
          { data: 'sisa', name: 'sisa' }
          // { data: 'opsi', name: 'opsi' }
        ]
    });

    $('#modal_detail_stok').on('shown.bs.modal', function (event) {
     var $button = $(event.relatedTarget);
     var tgl = $button.data('tgl');
     var nama_brg = $button.data('namabrg');
     var jenis = $button.data('jenis');
     
     console.log(tgl);
      $('input[name=tgl]').val(tgl);
      $('input[name=nama_brg]').val(nama_brg);
      $('input[name=jenis]').val(jenis);

      datatableStokMasuk.draw();
   });

  //  var datatableStokMasuk = $('#tb_detail_stok_masuk').DataTable({
  //       processing: true,
  //       serverSide: true,
  //       order: false,
  //       ajax: {
  //           type: 'POST',
  //           url: '{{ route('stok.detail_stok_mk') }}', 
  //           data: function (e) {
  //                 e._tgl = $('#Tgl').val();
  //                 e._nama_brg = $('#namaBrg').val();
  //                 e._jenis = $('#Jenis').val();
  //           }
  //       },
  //       columns: [
  //         { data: 'DT_RowIndex', name: 'DT_RowIndex' },
  //         { data: 'tgl', name: 'tgl' },
  //         { data: 'stok_awal', name: 'stok_awal' },
  //         { data: 'stok_masuk', name: 'stok_masuk' },
  //         { data: 'stok_keluar', name: 'stok_keluar' },
  //         { data: 'sisa', name: 'sisa' },
  //         { data: 'opsi', name: 'opsi' }
  //       ]
  //   });

  </script>
@endpush