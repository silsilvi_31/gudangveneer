@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Form Menu</h2>
              <ul class="nav navbar-right panel_toolbox">
                <a href=" {{ route('menu.index') }} " class="btn btn-sm btn-secondary">Back</i></a>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form id="" action="{{ route('menu.save') }}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                {{ csrf_field() }}
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Fitur <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="hidden" id="" name="id" required="required" class="form-control " autocomplete="off" value="{{ isset($id) ? $id : NULL }}" readonly>
                    <input type="text" id="" name="fitur" required="required" class="form-control " autocomplete="off" value="{{ isset($nama) ? $nama : NULL }}">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Icon <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="" name="icon" class="form-control " autocomplete="off" value="{{ isset($icon) ? $icon : NULL }}">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Link / Route
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="" name="route" class="form-control " autocomplete="off" value="{{ isset($route) ? $route : NULL }}">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Parent Id <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <select name="parent_id" id="" class="form-control " autocomplete="off">
                      @php
                          $menu = DB::table('menu')->get();
                          $parent_id_ = isset($parent_id) ? $parent_id : NULL
                      @endphp
                      <option value="">-</option>
                      @foreach ($menu as $item)
                        <option value="{{ $item->id }}" @if($item->id == $parent_id_) selected @endif>{{ $item->nama }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="ln_solid"></div>
                <div class="item form-group">
                  <div class="col-md-6 col-sm-6 offset-md-3">
                    <a href="{{ route('barang.index') }}" class="btn btn-warning" type="button">Cancel</a>
                    <button class="btn btn-danger" type="reset">Reset</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
    console.log('form');
    // var datatablePresensi = $('#tbJabatan').DataTable({
    //     processing: true,
    //     serverSide: true,
    //     ajax: {
    //         type: 'GET',
    //         url: '{{ route('jabatan.datatable') }}', 
    //     },
    //     columns: [
    //       { data: 'DT_RowIndex', name: 'DT_RowIndex' },
    //       { data: 'jabatan', name: 'jabatan' },
    //       { data: 'opsi', name: 'opsi' }
    //     ]
    // });

  </script>
@endpush