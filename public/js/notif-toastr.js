function notif(text, title, type, time = 500) {
    // console.log(type);
    var titleEx = (title === '' || title == undefined) ? type : title;

    if (type === 'success') {
        toastr.success(text, titleEx, {
            showDuration: time,
        });
    } else if (type === 'danger') {
        toastr.error(text, titleEx, {
            showDuration: time
        });
    } else if (type === 'warning') {
        toastr.warning(text, titleEx, {
            showDuration: time
        });
    } else if (type === 'info') {
        toastr.info(text, titleEx, {
            showDuration: time
        });
    }
}