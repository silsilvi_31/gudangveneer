<?php

Route::get('/tes', 'SampleController@menu_q');


Route::get('/lps', 'SjDefaultController@is_lps');

Route::get('/', 'LoginController@login')->name('login');
Route::post('login', 'LoginController@post_login')->name('login.post');
// Route::get('/add', 'LoginController@add_user')->name('l');

Route::get('logout', 'LoginController@logout')->name('logout');

Route::group(['prefix' => 'dashboard'], function () {
    Route::get('/', 'HomeController@index')->name('dashboard.index');
    
    //Manage
    Route::group(['prefix' => 'manage'], function () {
        //Karyawan
        Route::group(['prefix' => 'karyawan'], function () {
            Route::get('/', 'KaryawanController@index')->name('karyawan.index');
            Route::get('datatable', 'KaryawanController@datatable')->name('karyawan.datatable');
            Route::post('save', 'KaryawanController@save')->name('karyawan.save');
            Route::post('delete', 'KaryawanController@delete')->name('karyawan.delete');
        });
    });
    //Master
    Route::group(['prefix' => 'master'], function () {
        //Daftar akun
        Route::group(['prefix' => 'akun'], function () {
            Route::get('/', 'AkunController@index')->name('akun.index');
            Route::get('datatable', 'AkunController@datatable')->name('akun.datatable');
            Route::get('list', 'AkunController@akun_list')->name('akun.list');
            Route::get('form', 'AkunController@form')->name('akun.form');
            Route::post('save', 'AkunController@form')->name('akun.save');
        });

        //jenisbarang
        Route::group(['prefix' => 'jenisbarang'], function () {
            Route::get('/', 'JenisBarangController@index')->name('jenisbarang.index');
            Route::get('datatable', 'JenisBarangController@datatable')->name('jenisbarang.datatable');
            Route::get('form', 'JenisBarangController@form')->name('jenisbarang.form');
            Route::post('save', 'JenisBarangController@save')->name('jenisbarang.save');
            Route::get('form-edit/{id}', 'JenisBarangController@form_edit')->name('jenisbarang.form_edit');
            Route::post('update', 'JenisBarangController@update')->name('jenisbarang.update');
            Route::post('delete', 'JenisBarangController@delete')->name('jenisbarang.delete');
        });

        //barang
        Route::group(['prefix' => 'barang'], function () {
            Route::get('/', 'BarangController@index')->name('barang.index');
            Route::get('datatable', 'BarangController@datatable')->name('barang.datatable');
            Route::get('form', 'BarangController@form')->name('barang.form');
            Route::post('save', 'BarangController@save')->name('barang.save');
            Route::get('form-edit/{id}', 'BarangController@form_edit')->name('barang.form_edit');
            Route::post('update', 'BarangController@update')->name('barang.update');
            Route::post('delete', 'BarangController@delete')->name('barang.delete');
        });

       //bkkepada
        Route::group(['prefix' => 'bkkepada'], function () {
            Route::get('/', 'BkKepadaController@index')->name('bkkepada.index');
            Route::get('datatable', 'BkKepadaController@datatable')->name('bkkepada.datatable');
            Route::get('form', 'BkKepadaController@form')->name('bkkepada.form');
            Route::post('save', 'BkKepadaController@save')->name('bkkepada.save');
            Route::get('form-edit/{id}', 'BkKepadaController@form_edit')->name('bkkepada.form_edit');
            Route::post('update', 'BkKepadaController@update')->name('bkkepada.update');
            Route::post('delete', 'BkKepadaController@delete')->name('bkkepada.delete');
        });

        //satuan
        Route::group(['prefix' => 'satuan'], function () {
            Route::get('/', 'SatuanController@index')->name('satuan.index');
            Route::get('datatable', 'SatuanController@datatable')->name('satuan.datatable');
            Route::get('form', 'SatuanController@form')->name('satuan.form');
            Route::post('save', 'SatuanController@save')->name('satuan.save');
            Route::get('form-edit/{id}', 'SatuanController@form_edit')->name('satuan.form_edit');
            Route::post('update', 'SatuanController@update')->name('satuan.update');
            Route::post('delete', 'SatuanController@delete')->name('satuan.delete');
        });    

        //jabatan
        Route::group(['prefix' => 'jabatan'], function () {
            Route::get('/', 'JabatanController@index')->name('jabatan.index');
            Route::get('datatable', 'JabatanController@datatable')->name('jabatan.datatable');        
            Route::get('form', 'JabatanController@form')->name('jabatan.form');        
            Route::post('save', 'JabatanController@save')->name('jabatan.save');        
            Route::get('form-edit/{id}', 'JabatanController@form_edit')->name('jabatan.form_edit');        
            Route::post('update', 'JabatanController@update')->name('jabatan.update');        
            Route::post('delete', 'JabatanController@delete')->name('jabatan.delete');        
        });

        //kendaraan
        Route::group(['prefix' => 'kendaraan'], function ()  {
            Route::get('/', 'KendaraanController@index')->name('kendaraan.index');
            Route::get('datatable', 'KendaraanController@datatable')->name('kendaraan.datatable');
            Route::get('form', 'KendaraanController@form')->name('kendaraan.form');
            Route::post('save', 'KendaraanController@save')->name('kendaraan.save');
            Route::get('form-edit/{id}', 'KendaraanController@form_edit')->name('kendaraan.form_edit');
            Route::post('update', 'KendaraanController@update')->name('kendaraan.update');
            Route::post('delete', 'KendaraanController@delete')->name('kendaraan.delete');        
        });

        // Pelanggan
        Route::group(['prefix' => 'pelanggan'], function () {
            Route::get('/', 'PelangganController@index')->name('pelanggan.index');
            Route::get('datatable', 'PelangganController@datatable')->name('pelanggan.datatable');
            Route::get('form', 'PelangganController@form')->name('pelanggan.form');
            Route::post('save', 'PelangganController@save')->name('pelanggan.save');
            Route::get('form-edit/{id}', 'PelangganController@form_edit')->name('pelanggan.form_edit');
            Route::post('update', 'PelangganController@update')->name('pelanggan.update');
            Route::post('delete', 'PelangganController@delete')->name('pelanggan.delete');
        });

        // Bank
        Route::group(['prefix' => 'bank'], function (){
            Route::get('/', 'BankController@index')->name('bank.index');
            Route::get('datatable', 'BankController@datatable')->name('bank.datatable');
            Route::get('form', 'BankController@form')->name('bank.form');
            Route::post('save', 'BankController@save')->name('bank.save');
            Route::get('form_edit/{id}', 'BankController@form_edit')->name('bank.form_edit');
            Route::post('update', 'BankController@update')->name('bank.update');
            Route::post('delete', 'BankController@delete')->name('bank.delete');

        });
    });

    //Account
    Route::group(['prefix' => 'account'], function () {
        Route::get('/', 'AccountController@index')->name('account.index');  
        Route::get('datatable', 'AccountController@datatable')->name('account.datatable');
        Route::get('karyawan-list', 'AccountController@karyawan_list')->name('karyawan.list');
        Route::get('form', 'AccountController@form')->name('account.form');  
        Route::post('save', 'AccountController@save')->name('account.save');
        Route::post('update', 'AccountController@update')->name('account.update');
        Route::post('akses-update', 'AccountController@akses_update')->name('account_akses.update');
        Route::post('permis-update', 'AccountController@permis_update')->name('account_permis.update');
        Route::post('permis-get', 'AccountController@permis_get')->name('account_permis.get');

        Route::post('delete', 'AccountController@delete')->name('account.delete');  

        Route::get('/pimpinan', 'AccountController@index_pimpinan')->name('account_pimpinan.index');  
        Route::get('/data-pimpinan', 'AccountController@data_pimpinan')->name('account_pimpinan.datatable');  
        Route::get('/form-pimpinan/{id}', 'AccountController@create_account_pimpinan')->name('account_pimpinan.form');  
        Route::post('/pimpinan', 'AccountController@save_account_pimpinan')->name('account_pimpinan.save'); 
        Route::post('/create-pimpinan', 'AccountController@createv_account_pimpinan')->name('account_pimpinan.create');  
    });

    //Surat Jalan
    Route::group(['prefix' => 'sjdefault'], function () {
        Route::get('/', 'SjDefaultController@index')->name('sjdefault.index');
        Route::get('datatable', 'SjDefaultController@datatable')->name('sjdefault.datatable');
        Route::get('form', 'SjDefaultController@form')->name('sjdefault.form');
        Route::post('save', 'SjDefaultController@save')->name('sjdefault.save');
        Route::get('form_edit/{id}', 'SjDefaultController@form_edit')->name('sjdefault.form_edit');
        Route::get('delete', 'SjDefaultController@delete')->name('sjdefault.delete');
        Route::get('no','SjDefaultController@no_urut')->name('no_urut_sj');
        Route::get('datatable_brg','SjDefaultController@datatable_brg')->name('sjdefault.datatable_brg');
        Route::post('datatable_brg_detail','SjDefaultController@datatable_brg_detail')->name('sjdefault.datatable_brg_detail');
        Route::post('detele_item', 'SjDefaultController@delete_item')->name('sjdefault.delete_item');
        Route::get('nota/{id}', 'SjDefaultController@nota')->name('sjdefault.nota');
        Route::get('sj/{id}', 'SjDefaultController@sj')->name('sjdefault.sj');
        Route::post('barang_nota', 'SjDefaultController@barang_nota')->name('sjdefault.barang_nota');
        Route::post('total', 'SjDefaultController@total')->name('sjdefault.total');
        Route::post('add_pelanggan', 'SjDefaultController@add_pelanggan')->name('sjdefault.add_pelanggan');
        Route::post('select_pelanggan', 'SjDefaultController@select_pelanggan')->name('sjdefault.select_pelanggan');
        Route::get('excel_sj/{tgl}','SjDefaultController@excel_sj')->name('sjdefault.excel_sj');
        Route::post('delete_tgl', 'sjdefaultController@delete_tgl')->name('sjdefault.delete_tgl');
    });

    // cek
    Route::group(['prefix' => 'cek-sj'], function () {
        Route::get('/', 'CekSjController@index')->name('ceksj.index');
        Route::get('datatable', 'CekSjController@datatable')->name('ceksj.datatable');
        Route::post('datatable_sj', 'CekSjController@datatable_sj')->name('ceksj.datatable_sj');
        Route::post('acc_sj', 'CekSjController@acc_sj')->name('ceksj.acc_sj');
        Route::get('nota/{id}', 'CekSjController@nota')->name('ceksj.nota');
        Route::get('sj/{id}', 'CekSjController@sj')->name('ceksj.sj');
        
    });

    //Izin Kendaraan
    Route::group(['prefix' => 'izin_keluar'], function () {
        Route::get('/', 'IzinKeluarController@index')->name('izin_keluar.index');
        Route::get('datatable', 'IzinKeluarController@datatable')->name('izin_keluar.datatable');
        Route::get('datatable_kendaraan','IzinKeluarController@datatable_kendaraan')->name('izin_keluar.datatable_kendaraan');
        Route::get('form', 'IzinKeluarController@form')->name('izin_keluar.form');
        Route::post('save', 'IzinKeluarController@save')->name('izin_keluar.save');
        Route::get('form_edit/{id}', 'IzinKeluarController@form_edit')->name('izin_keluar.form_edit');
        Route::post('update', 'IzinKeluarController@update')->name('izin_keluar.update');
        Route::post('delete', 'IzinKeluarController@delete')->name('izin_keluar.delete');
        Route::get('kendaraan_kembali', 'IzinKeluarController@kendaraan_kembali')->name('izin_keluar.kendaraan_kembali');
        Route::post('izin_kembali', 'IzinKeluarController@izin_kembali')->name('izin_keluar.izin_kembali');
    });

    //Permintaan Barang
    Route::group(['prefix' => 'permintaan_brg'], function () {
        Route::get('/', 'PermintaanBrgController@index')->name('permintaanBrg.index');
        Route::get('datatable', 'PermintaanBrgController@datatable')->name('permintaanBrg.datatable');
        Route::post('datatable_brg', 'PermintaanBrgController@datatable_brg')->name('permintaanBrg.datatable_brg');
        Route::get('datatable_item', 'PermintaanBrgController@datatable_item')->name('permintaanBrg.datatable_item');
        Route::get('form', 'PermintaanBrgController@form')->name('permintaanBrg.form');
        Route::post('save', 'PermintaanBrgController@save')->name('permintaanBrg.save');
        Route::get('form_edit/{id}', 'PermintaanBrgController@form_edit')->name('permintaanBrg.form_edit');
        Route::post('update', 'PermintaanBrgController@update')->name('permintaanBrg.update');
        Route::get('delete', 'PermintaanBrgController@delete')->name('permintaanBrg.delete');
        Route::post('delete_item', 'PermintaanBrgController@delete_item')->name('permintaanBrg.delete_item');       
        Route::get('daftar_belanja', 'PermintaanBrgController@daftar_belanja')->name('permintaanBrg.daftar_belanja');
        Route::get('datatable_daftarBelanja', 'PermintaanBrgController@datatable_daftarBelanja')->name('permintaanBrg.datatable_daftarBelanja');
        Route::post('is_beli', 'PermintaanBrgController@is_beli')->name('permintaanBrg.is_beli');
        Route::get('print/{id}', 'PermintaanBrgController@print')->name('permintaanBrg.print');
        Route::post('update_qty', 'PermintaanBrgController@update_qty')->name('permintaanBrg.update_qty');
        Route::get('no_bm', 'PermintaanBrgController@no_bm')->name('permintaanBrg.no_bm');
        Route::post('detail_po', 'PermintaanBrgController@detail_po')->name('permintaanBrg.detail_po');
        Route::post('acc_item', 'PermintaanBrgController@acc_item')->name('permintaanBrg.acc_item');
        Route::post('buat_bm', 'PermintaanBrgController@buat_bm')->name('permintaanBrg.buat_bm');
    });

    // Cek permintaan Barang
    Route::group(['prefix' => 'cek_permintaan_brg'], function () {
        Route::get('/', 'CekPermintaanBrgController@index')->name('cekPermintaanBrg.index');
        Route::get('datatable', 'CekPermintaanBrgController@datatable')->name('cekPermintaanBrg.datatable');
        Route::post('datatable_detail', 'CekPermintaanBrgController@datatable_detail')->name('cekPermintaanBrg.datatable_detail');
        Route::post('acc_item', 'CekPermintaanBrgController@acc_item')->name('cekPermintaanBrg.acc_item');
        Route::post('delete', 'CekPermintaanBrgController@delete')->name('cekPermintaanBrg.delete');
        Route::post('konfirmasi_permintaan', 'CekPermintaanBrgController@konfirmasi_permintaan')->name('cekPermintaanBrg.konfirmasi_permintaan');
    });

    //jurnal
    Route::group(['prefix' => 'jurnal'], function () {
      Route::get('/', 'JurnalController@index')->name('jurnal.index');
      Route::post('datatable', 'JurnalController@datatable')->name('jurnal.datatable');
      Route::get('excel/{tgl}','JurnalController@excel_jurnal')->name('jurnal.excel');
      Route::post('export-rmv', 'JurnalController@export_rmv')->name('jurnal_export.rmv');      
      Route::post('export_excel_count', 'JurnalController@export_excel_count')->name('jurnal.export_excel_count');
    });

    // bk sampah
    Route::group(['prefix' => 'bk'], function () {
        Route::get('/', 'BkController@index')->name('bk.index');
        Route::get('datatable', 'BkController@datatable')->name('bk.datatable');
        Route::get('form', 'BkController@form')->name('bk.form');
        Route::get('list-brg', 'BkController@list_brg')->name('bk_brg.list');
        Route::post('save', 'BkController@save')->name('bk.save');
        Route::post('item', 'BkController@item_bk')->name('bk.item');
        Route::post('delete-item', 'BkController@delete_item')->name('bk_item.delete');
        Route::post('delete', 'BkController@delete')->name('bk.delete');
        Route::get('form_editt/{no_bk}', 'BkController@form_edit')->name('bk.form_edit');
        Route::get('max_no_bk', 'BkController@max_no_bk')->name('bk.max_no_bk');
        Route::post('total_harga', 'BkController@get_total_harga')->name('total_harga.json');
        Route::get('print/{no_bk}', 'BkController@print')->name('bk.print');
        Route::post('get-no', 'BkController@get_no')->name('get_no.print');
    });

    //bk bahan bakar
    Route::group(['prefix' => 'bahanbakar'], function () {
        Route::get('/', 'BkBahanBakarController@index')->name('bahanbakar.index');
        Route::get('datatable', 'BkBahanBakarController@datatable')->name('bahanbakar.datatable');
        Route::get('form', 'BkBahanBakarController@form')->name('bahanbakar.form');
        Route::get('list-brg', 'BkBahanBakarController@list_brg')->name('bahanbakar_brg.list');
        Route::post('save', 'BkBahanBakarController@save')->name('bahanbakar.save');
        Route::post('item', 'BkBahanBakarController@item_bk')->name('bahanbakar.item');
        Route::post('delete-item', 'BkBahanBakarController@delete_item')->name('bahanbakar_item.delete');
        Route::post('delete', 'BkBahanBakarController@delete')->name('bahanbakar.delete');
        Route::get('form_editt/{no_bk}', 'BkBahanBakarController@form_edit')->name('bahanbakar.form_edit');
        Route::get('max_no_bk', 'BkBahanBakarController@max_no_bk')->name('bahanbakar.max_no_bk');
        Route::post('total_harga', 'BkBahanBakarController@get_total_harga')->name('bahanbakar_total.json');
        Route::get('print/{no_bk}', 'BkBahanBakarController@print')->name('bahanbakar.print');
        Route::post('get_no_bahanbakar', 'BkBahanBakarController@get_no_bahanbakar')->name('get_no_bahanbakar.print');
    });

    // Gudang (pindah triplek)
    Route::group(['prefix' => 'pindahtriplek'], function () {
        Route::get('/', 'PindahTriplekController@index')->name('pindahtriplek.index');
        Route::get('datatable', 'PindahTriplekController@datatable')->name('pindahtriplek.datatable');
        Route::get('form', 'PindahTriplekController@form')->name('pindahtriplek.form');
        Route::get('list-brg', 'PindahTriplekController@list_brg')->name('pindah_brg.list');
        Route::post('save', 'PindahTriplekController@save')->name('pindahtriplek.save');
        Route::post('item', 'PindahTriplekController@item_bk')->name('pindahtriplek.item');
        Route::post('delete-item', 'PindahTriplekController@delete_item')->name('pindahtriplek_item.delete');
        Route::post('delete', 'PindahTriplekController@delete')->name('pindahtriplek.delete');
        Route::get('form_editt/{no_bk}', 'PindahTriplekController@form_edit')->name('pindahtriplek.form_edit');
        Route::get('max_no_bk', 'PindahTriplekController@max_no_bk')->name('pindahtriplek.max_no_bk');
        Route::post('total_harga', 'PindahTriplekController@get_total_harga')->name('total_harga_pt.json');
        Route::get('print/{no_bk}', 'PindahTriplekController@print')->name('pindahtriplek.print');
        Route::post('get_no_pindah', 'PindahTriplekController@get_no_pindah')->name('get_no_pindah.print');
    });

    // Kayu Bakar
    Route::group(['prefix' => 'kayubakar'], function () {
        Route::get('/', 'BkKayuBakarController@index')->name('kayubakar.index');
        Route::get('datatable', 'BkKayuBakarController@datatable')->name('kayubakar.datatable');
        Route::get('form', 'BkKayuBakarController@form')->name('kayubakar.form');
        Route::post('list-brg', 'BkKayuBakarController@list_brg')->name('kayubakar_brg.list');
        Route::post('save', 'BkKayuBakarController@save')->name('kayubakar.save');
        Route::post('item', 'BkKayuBakarController@item_bk')->name('kayubakar.item');
        Route::post('delete-item', 'BkKayuBakarController@delete_item')->name('kayubakar_item.delete');
        Route::post('delete', 'BkKayuBakarController@delete')->name('kayubakar.delete');
        Route::get('form_editt/{no_bk}', 'BkKayuBakarController@form_edit')->name('kayubakar.form_edit');
        Route::get('max_no_bk', 'BkKayuBakarController@max_no_bk')->name('kayubakar.max_no_bk');
        Route::post('total_harga', 'BkKayuBakarController@get_total_harga')->name('kayubakar_total.json');
        Route::get('print/{no_bk}', 'BkKayuBakarController@print')->name('kayubakar.print');
        Route::post('get_no_kayubakar', 'BkKayuBakarController@get_no_kayubakar')->name('get_no_kayubakar.print');

    });

    // Tembel
    Route::group(['prefix' => 'bktembel'], function () {
        Route::get('/', 'BkTembelController@index')->name('bktembel.index');
        Route::get('datatable', 'BkTembelController@datatable')->name('bktembel.datatable');
        Route::get('form', 'BkTembelController@form')->name('bktembel.form');
        Route::get('list-brg', 'BkTembelController@list_brg')->name('bktembel.list');
        Route::post('get-tembel', 'BkTembelController@get_nama_tembel')->name('get_tembel.json');
        Route::post('save', 'BkTembelController@save')->name('bktembel.save');
        Route::post('item', 'BkTembelController@item_bk')->name('bktembel.item');
        Route::post('delete-item', 'BkTembelController@delete_item')->name('bktembel_item.delete');
        Route::post('delete', 'BkTembelController@delete')->name('bktembel.delete');
        Route::get('form_edit/{no_bk}', 'BkTembelController@form_edit')->name('bktembel.form_edit');
        Route::get('max_no_bk', 'BkTembelController@max_no_bk')->name('bktembel.max_no_bk');
        Route::post('total_harga', 'BkTembelController@get_total_harga')->name('bktembel_total_harga.json');
        Route::get('print/{no_bk}', 'BkTembelController@print')->name('bktembel.print');
        Route::post('get_no_tembel', 'BkTembelController@get_no_tembel')->name('get_no_tembel.print');
        Route::post('add_vnr', 'BkTembelController@add_vnr')->name('bktembel.add_vnr');
    });

    //bk kandang
    Route::group(['prefix' => 'bkkandang'], function () {
        Route::get('/', 'BkKandangController@index')->name('bkkandang.index');
        Route::get('datatable', 'BkKandangController@datatable')->name('bkkandang.datatable');
        Route::get('form', 'BkKandangController@form')->name('bkkandang.form');
        Route::get('list-brg', 'BkKandangController@list_brg')->name('bkkandang.list');
        Route::post('get-tembel', 'BkKandangController@get_nama_tembel')->name('get_kandang.json');
        Route::post('save', 'BkKandangController@save')->name('bkkandang.save');
        Route::post('item', 'BkKandangController@item_bk')->name('bkkandang.item');
        Route::post('delete-item', 'BkKandangController@delete_item')->name('bkkandang_item.delete');
        Route::post('delete', 'BkKandangController@delete')->name('bkkandang.delete');
        Route::get('form_edit/{no_bk}', 'BkKandangController@form_edit')->name('bkkandang.form_edit');
        Route::get('max_no_bk', 'BkkandangController@max_no_bk')->name('bkkandang.max_no_bk');
        Route::post('total_harga', 'BkKandangController@get_total_harga')->name('bkkandang_total_harga.json');
        Route::get('print/{no_bk}', 'BkKandangController@print')->name('bkkandang.print');
    });

    // pindah veneer
    Route::group(['prefix' => 'veneer'], function () {
        Route::get('/', 'BkVeneer@index')->name('veneer.index');
        // Route::get('datatable', 'BkVeneerController@datatable')->name('veneer.datatable');
        Route::get('form', 'BkVeneerController@form')->name('veneer.form');
        Route::get('list-brg', 'BkVeneerController@list_brg')->name('veneer.list');
        Route::post('get-tembel', 'BkVeneerController@get_nama_veneer')->name('get_veneer.json');
        Route::post('save', 'BkVeneerController@save')->name('veneer.save');
        Route::post('item', 'BkVeneerController@item_bk')->name('veneer.item');
        Route::post('delete-item', 'BkVeneerController@delete_item')->name('veneer_item.delete');
        Route::post('delete', 'BkVeneerController@delete')->name('veneer.delete');
        Route::get('form_edit/{no_bk}', 'BkVeneerController@form_edit')->name('veneer.form_edit');
        Route::get('max_no_bk', 'BkVeneerController@max_no_bk')->name('veneer.max_no_bk');
        Route::post('total_harga', 'BkVeneerController@get_total_harga')->name('veneer.json');
        Route::get('print/{no_bk}', 'BkVeneerController@print')->name('veneer.print');
        Route::post('get_no_veneer', 'BkVeneerController@get_no_veneer')->name('get_no_veneer.print');
        Route::post('add_vnr', 'BkVeneerController@add_vnr')->name('veneer.add_vnr');
    });

    // Gudang Lain-lain
    Route::group(['prefix' => 'gdlain'], function () {
        Route::get('/', 'BkGdlainController@index')->name('gdlain.index');
        // Route::get('datatable', 'BkGdlainController@datatable')->name('gdlain.datatable');
        Route::get('form', 'BkGdlainController@form')->name('gdlain.form');
        Route::get('list-brg', 'BkGdlainController@list_brg')->name('gdlain.list');
        // Route::post('get-tembel', 'BkGdlainController@get_nama_gdlain')->name('get_gdlain.json');
        Route::post('save', 'BkGdlainController@save')->name('gdlain.save');
        Route::post('item', 'BkGdlainController@item_bk')->name('gdlain.item');
        Route::post('delete-item', 'BkGdlainController@delete_item')->name('gdlain_item.delete');
        Route::post('delete', 'BkGdlainController@delete')->name('gdlain.delete');
        Route::get('form_edit/{no_bk}', 'BkGdlainController@form_edit')->name('gdlain.form_edit');
        Route::get('max_no_bk', 'BkGdlainController@max_no_bk')->name('gdlain.max_no_bk');
        Route::post('total_harga', 'BkGdlainController@get_total_harga')->name('gdlain.json');
        Route::get('print/{no_bk}', 'BkGdlainController@print')->name('gdlain.print');
        Route::post('get_no_gdlain', 'BkGdlainController@get_no_gdlain')->name('get_no_gdlain.print');
    });

    //barang kayubakar
    Route::group(['prefix' => 'hargakayubakar'], function () {
        Route::get('/', 'HargaKayuBakarController@index')->name('hargakayubakar.index');
        Route::get('datatable', 'HargaKayuBakarController@datatable')->name('hargakayubakar.datatable');
        Route::get('form', 'HargaKayuBakarController@form')->name('hargakayubakar.form');
        Route::post('save', 'HargaKayuBakarController@save')->name('hargakayubakar.save');
        Route::get('form-edit/{id}', 'HargaKayuBakarController@form_edit')->name('hargakayubakar.form_edit');
        Route::post('update', 'HargaKayuBakarController@update')->name('hargakayubakar.update');
        Route::post('delete', 'HargaKayuBakarController@delete')->name('hargakayubakar.delete');
        Route::post('add_kepada', 'HargaKayuBakarController@add_kepada')->name('hargakayubakar.add_kepada');
    });

    //mesin
    Route::group(['prefix' => 'mesin'], function () {
        Route::get('/', 'BkMesinController@index')->name('mesin.index');
        Route::get('datatable', 'BkMesinController@datatable')->name('mesin.datatable');
        Route::get('form', 'BkMesinController@form')->name('mesin.form');
        Route::post('list-brg', 'BkMesinController@list_brg')->name('mesin_brg.list');     
        Route::post('save', 'BkMesinController@save')->name('mesin.save');
        Route::post('item', 'BkMesinController@item_bk')->name('mesin.item');
        Route::post('delete-item', 'BkMesinController@delete_item')->name('mesin_item.delete');
        Route::post('delete', 'BkMesinController@delete')->name('mesin.delete');
        Route::get('form_editt/{no_bk}', 'BkMesinController@form_edit')->name('mesin.form_edit');
        Route::get('max_no_bk', 'BkMesinController@max_no_bk')->name('mesin.max_no_bk');
        Route::post('total_harga', 'BkMesinController@get_total_harga')->name('mesin_total.json');
        Route::get('print/{no_bk}', 'BkMesinController@print')->name('mesin.print');
        Route::post('get_no_mesin', 'BkMesinController@get_no_mesin')->name('get_no_mesin.print');
        Route::post('add_kepada', 'BkMesinController@add_kepada')->name('mesin.add_kepada');
    });
    
    //lain-lain
    Route::group(['prefix' => 'lain'], function () {
        Route::get('/', 'BkLainController@index')->name('lain.index');
        Route::get('datatable', 'BkLainController@datatable')->name('lain.datatable');
        Route::get('form', 'BkLainController@form')->name('lain.form');
        Route::post('list-brg', 'BkLainController@list_brg')->name('lain_brg.list');     
        Route::post('save', 'BkLainController@save')->name('lain.save');
        Route::post('item', 'BkLainController@item_bk')->name('lain.item');
        Route::post('delete-item', 'BkLainController@delete_item')->name('lain_item.delete');
        Route::post('delete', 'BkLainController@delete')->name('lain.delete');
        Route::get('form_editt/{no_bk}', 'BkLainController@form_edit')->name('lain.form_edit');
        Route::get('max_no_bk', 'BkLainController@max_no_bk')->name('lain.max_no_bk');
        Route::post('total_harga', 'BkLainController@get_total_harga')->name('lain_total.json');
        Route::get('print/{no_bk}', 'BkLainController@print')->name('lain.print');
        Route::post('add_kepada', 'BkLainController@add_kepada')->name('lain.add_kepada');
        Route::post('add_barang', 'BkLainController@add_barang')->name('lain.add_barang');
        Route::post('add_satuan', 'BkLainController@add_satuan')->name('lain.add_satuan');
        Route::post('get_no_lain', 'BkLainController@get_no_lain')->name('get_no_lain.print');
    });

    Route::group(['prefix' => 'setting'], function () {
        Route::group(['prefix' => 'menu'], function () {
            Route::get('/', 'MenuController@index')->name('menu.index');
            Route::get('datatable', 'MenuController@datatable')->name('menu.datatable'); 
            Route::get('form', 'MenuController@form')->name('menu.form');
            Route::post('save', 'MenuController@save')->name('menu.save');
            Route::get('edit/{id}', 'MenuController@edit')->name('menu.edit');
            Route::post('delete', 'MenuController@delete')->name('menu.delete');
        });

        Route::group(['prefix' => 'akses'], function () {
            Route::get('/', 'MenuController@akses')->name('akses.index');
            Route::get('datatable', 'MenuController@datatable_akses')->name('akses.datatable');
            Route::get('form', 'MenuController@akses_form')->name('akses.form');
            Route::post('save', 'MenuController@akses_save')->name('akses.save');
            Route::post('delete', 'MenuController@akses_delete')->name('akses.delete');
            Route::get('list-akses', 'MenuController@list_akses')->name('list_akses.datatable');
            Route::get('list-fitur', 'MenuController@list_fitur')->name('list_fitur.datatable');
            Route::post('item-fitur', 'MenuController@item_fitur')->name('item_fitur.datatable');
            Route::post('delete-fitur', 'MenuController@delete_fitur')->name('item_fitur.delete');
        });
    });


    //PO Tidak Langsung
    Route::group(['prefix' => 'PoTidakLangsung'], function (){
        Route::get('/', 'PoTidakLangsungController@index')->name('poTidakLangsung.index');
        Route::get('datatable', 'PoTidakLangsungController@datatable')->name('poTidakLangsung.datatable');
        Route::get('form', 'PoTidakLangsungController@form')->name('poTidakLangsung.form');
        Route::post('save', 'PoTidakLangsungController@save')->name('poTidakLangsung.save');
        Route::get('form_edit/{id}', 'PoTidakLangsungController@form_edit')->name('poTidakLangsung.form_edit');
        Route::post('update', 'PoTidakLangsungController@update')->name('poTidakLangsung.update');
        Route::get('delete', 'PoTidakLangsungController@delete')->name('poTidakLangsung.delete');
    });

    //SJ Barang
    Route::group(['prefix' => 'sjBarang'], function () {
        Route::get('/', 'SjBarangController@index')->name('sjBarang.index');
        Route::get('datatable', 'SjBarangController@datatable')->name('sjBarang.datatable');
        Route::get('form', 'SjBarangController@form')->name('sjBarang.form');
        Route::post('save', 'SjBarangController@save')->name('sjBarang.save');
        Route::get('form_edit/{id}', 'SjBarangController@form_edit')->name('sjBarang.form_edit');
        Route::get('delete', 'SjBarangController@delete')->name('sjBarang.delete');
        Route::get('datatable_brg','SjBarangController@datatable_brg')->name('sjBarang.datatable_brg');
        Route::post('datatable_item','SjBarangController@datatable_item')->name('sjBarang.datatable_item');
        Route::post('detele_item', 'SjBarangController@delete_item')->name('sjBarang.delete_item');
        // Route::get('nota/{id}', 'SjBarangController@nota')->name('sjBarang.nota');
        // Route::get('sj/{id}', 'SjBarangController@sj')->name('sjBarang.sj');
        Route::post('barang_nota', 'SjBarangController@barang_nota')->name('sjBarang.barang_nota');
        Route::post('total', 'SjBarangController@total')->name('sjBarang.total');
        Route::post('add_pelanggan', 'SjBarangController@add_pelanggan')->name('sjBarang.add_pelanggan');
        Route::post('add_brg', 'SjBarangController@add_brg')->name('sjBarang.add_brg');
        Route::post('select_pelanggan', 'SjBarangController@select_pelanggan')->name('sjBarang.select_pelanggan');
        Route::get('excel_sj/{tgl}','SjBarangController@excel_sj')->name('sjBarang.excel_sj');
    });

    Route::post('get_no_bm', 'BmController@get_no_bm')->name('get_no_bm.json');

    //Barang Masuk
    Route::group(['prefix' => 'bm'], function () {
        Route::get('/', 'BmController@index')->name('bm.index');
        Route::get('datatable', 'BmController@datatable')->name('bm.datatable');
        Route::get('datatable_brg', 'BmController@datatable_brg')->name('bm.datatable_brg');
        Route::post('datatable_item', 'BmController@datatable_item')->name('bm.datatable_item');
        Route::get('form', 'BmController@form')->name('bm.form');
        Route::post('save', 'BmController@save')->name('bm.save');
        Route::get('form_edit/{id}', 'BmController@form_edit')->name('bm.form_edit');
        Route::get('delete_bm', 'BmController@delete_bm')->name('bm.delete_bm');     
        Route::get('no_bm', 'BmController@no_bm')->name('bm.no_bm');
        Route::post('add_barang', 'BmController@add_barang')->name('bm.add_barang') ;
        Route::post('delete_item', 'BmController@delete_item')->name('bm.delete_item');
        Route::post('total', 'BmController@total')->name('bm.total');
        Route::get('print/{id}', 'BmController@print')->name('bm.print');
        Route::post('print_bm_count', 'BmController@print_bm_count')->name('bm.print_bm_count');
        Route::post('add_suplier', 'BmController@add_suplier')->name('bm.add_suplier');

                    
    });

    //Rekap
    Route::group(['prefix' => 'rekap'], function () {
        Route::get('/', 'RekapController@index')->name('rekap.index');
        Route::get('excel_bk/{tgl}','RekapController@excel_bk')->name('rekap.excel_bk');
        Route::post('delete_tgl', 'RekapController@delete_tgl')->name('rekap.delete_tgl');
    });

    Route::group(['prefix' => 'cek-bk'], function () {
        Route::get('/', 'CekBkController@index')->name('cekbk.index');
        Route::get('datatable', 'CekBkController@datatable')->name('cekbk.datatable');
        Route::post('datatable_bk', 'CekBkController@datatable_bk')->name('cekbk.datatable_bk');
        Route::post('acc_bk', 'CekBkController@acc_bk')->name('cekbk.acc_bk');
        Route::get('nota/{id}', 'CekBkController@nota')->name('cekbk.nota');
        Route::get('bk/{id}', 'CekBkController@bk')->name('cekbk.bk');
        Route::get('print/{no_bk}', 'CekBkController@print')->name('cekbk.print');
        Route::post('batal', 'CekBkController@batal')->name('cekbk.batal');
        Route::post('get-kpd-lh', 'CekBkController@get_kpd_lh')->name('get_kpd_lh.json');
    });

    Route::post('get-no-bk', 'BkController@get_no_bk')->name('get_no_bk.json');
    Route::post('get-no-bk_banding', 'BkControllerbanding@get_no_bk_banding')->name('get_no_bk_banding.json');

    // Penjarangan
    Route::group(['prefix' => 'penjarangan'], function () {
        Route::get('/', 'BkPenjaranganController@index')->name('penjarangan.index');
        Route::get('datatable', 'BkPenjaranganController@datatable')->name('penjarangan.datatable');
        Route::get('form', 'BkPenjaranganController@form')->name('penjarangan.form');
        Route::get('list-brg', 'BkPenjaranganController@list_brg')->name('penjarangan.list');
        // Route::post('get-tembel', 'BkPenjaranganController@get_nama_penjarangan')->name('get_penjarangan.json');
        Route::post('save', 'BkPenjaranganController@save')->name('penjarangan.save');
        Route::post('item', 'BkPenjaranganController@item_bk')->name('penjarangan.item');
        Route::post('delete-item', 'BkPenjaranganController@delete_item')->name('penjarangan_item.delete');
        Route::post('delete', 'BkPenjaranganController@delete')->name('penjarangan.delete');
        Route::get('form_edit/{no_bk}', 'BkPenjaranganController@form_edit')->name('penjarangan.form_edit');
        Route::get('max_no_bk', 'BkPenjaranganController@max_no_bk')->name('penjarangan.max_no_bk');
        Route::post('total_harga', 'BkPenjaranganController@get_total_harga')->name('penjarangan_total_harga.json');
        Route::get('print/{no_bk}', 'BkPenjaranganController@print')->name('penjarangan.print');
        Route::get('list-lahan', 'BkPenjaranganController@list_lahan')->name('list_lahan.datatable');
        Route::get('list-pekerja', 'BkPenjaranganController@list_pekerja')->name('list_pekerja.datatable');
        Route::post('get_lahan', 'BkPenjaranganController@get_lahan')->name('get_lahan.json');
        Route::post('get_pekerja', 'BkPenjaranganController@get_pekerja')->name('get_pekerja.json');
        
        Route::post('pj_lahan', 'BkPenjaranganController@pj_lahan')->name('pj_lahan.delete');
        Route::post('pj_pekerja', 'BkPenjaranganController@pj_pekerja')->name('pj_pekerja.delete');
        Route::post('simpan_lahan', 'BkPenjaranganController@simpan_lahan')->name('simpan_lahan.save');
        Route::post('simpan_pekerja', 'BkPenjaranganController@simpan_pekerja')->name('simpan_pekerja.save');
        Route::post('get_no_penjarangan', 'BkPenjaranganController@get_no_penjarangan')->name('get_no_penjarangan.print');
        Route::post('add_pkj', 'BkPenjaranganController@add_pkj')->name('penjarangan.add_pkj');
    });

        //Bk Gd Kayu
        Route::group(['prefix' => 'gdkayu'], function () {
            Route::get('/', 'BkGdKayuController@index')->name('gdkayu.index');
            Route::get('datatable', 'BkGdKayuController@datatable')->name('gdkayu.datatable');
            Route::get('form', 'BkGdKayuController@form')->name('gdkayu.form');
            Route::post('list-brg', 'BkGdKayuController@list_brg')->name('gdkayu_brg.list');     
            Route::post('save', 'BkGdKayuController@save')->name('gdkayu.save');
            Route::post('item', 'BkGdKayuController@item_bk')->name('gdkayu.item');
            Route::post('delete-item', 'BkGdKayuController@delete_item')->name('gdkayu_item.delete');
            Route::post('delete', 'BkGdKayuController@delete')->name('gdkayu.delete');
            Route::get('form_editt/{no_bk}', 'BkGdKayuController@form_edit')->name('gdkayu.form_edit');
            Route::get('max_no_bk', 'BkGdKayuController@max_no_bk')->name('gdkayu.max_no_bk');
            Route::post('total_harga', 'BkGdKayuController@get_total_harga')->name('gdkayu_total.json');
            Route::get('print/{no_bk}', 'BkGdKayuController@print')->name('gdkayu.print');
            Route::post('add_kepada', 'BkGdKayuController@add_kepada')->name('gdkayu.add_kepada');
            Route::post('add_barang', 'BkGdKayuController@add_barang')->name('gdkayu.add_barang');
            Route::post('add_satuan', 'BkGdKayuController@add_satuan')->name('gdkayu.add_satuan');
            Route::post('get_no_gdkayu', 'BkGdKayuController@get_no_gdkayu')->name('get_no_gdkayu.print');

        });

        //hanero gaji
        Route::group(['prefix' => 'hanero'], function () {
            Route::get('/', 'BkHaneroController@index')->name('hanero.index');
            Route::get('datatable', 'BkHaneroController@datatable')->name('hanero.datatable');
            Route::get('form', 'BkHaneroController@form')->name('hanero.form');
            Route::post('list-brg', 'BkHaneroController@list_brg')->name('hanero_brg.list');     
            Route::post('save', 'BkHaneroController@save')->name('hanero.save');
            Route::post('item', 'BkHaneroController@item_bk')->name('hanero.item');
            Route::post('delete-item', 'BkHaneroController@delete_item')->name('hanero_item.delete');
            Route::post('delete', 'BkHaneroController@delete')->name('hanero.delete');
            Route::get('form_editt/{no_bk}', 'BkHaneroController@form_edit')->name('hanero.form_edit');
            Route::get('max_no_bk', 'BkHaneroController@max_no_bk')->name('hanero.max_no_bk');
            Route::post('total_harga', 'BkHaneroController@get_total_harga')->name('hanero_total.json');
            Route::get('print/{no_bk}', 'BkHaneroController@print')->name('hanero.print');
            Route::post('add_pekerja', 'BkHaneroController@add_pekerja')->name('hanero.add_pekerja');
            Route::post('get_no_hanero', 'BkHaneroController@get_no_hanero')->name('get_no_hanero.print');
    });

        //hanero barang
        Route::group(['prefix' => 'hanerobarang'], function () {
            Route::get('/', 'BkHaneroBarangController@index')->name('hanerobarang.index');
            // Route::get('datatable', 'BkHaneroBarangController@datatable')->name('hanero.datatable');
            Route::get('form', 'BkHaneroBarangController@form')->name('hanerobarang.form');
            Route::post('list-brg', 'BkHaneroBarangController@list_brg')->name('hanerobarang_brg.list');     
            Route::post('save', 'BkHaneroBarangController@save')->name('hanerobarang.save');
            Route::post('item', 'BkHaneroBarangController@item_bk')->name('hanerobarang.item');
            Route::post('delete-item', 'BkHaneroBarangController@delete_item')->name('hanerobarang_item.delete');
            Route::post('delete', 'BkHaneroBarangController@delete')->name('hanerobarang.delete');
            Route::get('form_editt/{no_bk}', 'BkHaneroBarangController@form_edit')->name('hanerobarang.form_edit');
            Route::get('max_no_bk', 'BkHaneroBarangController@max_no_bk')->name('hanerobarang.max_no_bk');
            Route::post('total_harga', 'BkHaneroBarangController@get_total_harga')->name('hanerobarang_total.json');
            Route::get('print/{no_bk}', 'BkHaneroBarangController@print')->name('hanerobarang.print');
            Route::post('get_no_hanerobarang', 'BkHaneroController@get_no_hanerobarang')->name('get_no_hanerobarang.print');
    });

        //pak tri
        Route::group(['prefix' => 'paktri'], function () {
            Route::get('/', 'BkPakTriController@index')->name('paktri.index');
            Route::get('datatable', 'BkPakTriController@datatable')->name('paktri.datatable');
            Route::get('form', 'BkPakTriController@form')->name('paktri.form');
            Route::post('list-brg', 'BkPakTriController@list_brg')->name('paktri_brg.list');     
            Route::post('save', 'BkPakTriController@save')->name('paktri.save');
            Route::post('item', 'BkPakTriController@item_bk')->name('paktri.item');
            Route::post('delete-item', 'BkPakTriController@delete_item')->name('paktri_item.delete');
            Route::post('delete', 'BkPakTriController@delete')->name('paktri.delete');
            Route::get('form_editt/{no_bk}', 'BkPakTriController@form_edit')->name('paktri.form_edit');
            Route::get('max_no_bk', 'BkPakTriController@max_no_bk')->name('paktri.max_no_bk');
            Route::post('total_harga', 'BkPakTriController@get_total_harga')->name('paktri_total.json');
            Route::get('print/{no_bk}', 'BkPakTriController@print')->name('paktri.print');
            Route::post('get_no_paktri', 'BkPakTriController@get_no_paktri')->name('get_no_paktri.print');
    });

    //Surat Jalan untuk admin
    Route::group(['prefix' => 'print_sj'], function () {
        Route::get('/', 'PrintSjController@index')->name('PrintSj.index');
        Route::get('datatable', 'PrintSjController@datatable')->name('PrintSj.datatable');
        Route::get('form', 'PrintSjController@form')->name('PrintSj.form');
        Route::post('save', 'PrintSjController@save')->name('PrintSj.save');
        Route::get('form_edit/{id}', 'PrintSjController@form_edit')->name('PrintSj.form_edit');
        Route::get('delete', 'PrintSjController@delete')->name('PrintSj.delete');
        Route::get('no','PrintSjController@no_urut')->name('no_urut_sj');
        Route::get('datatable_brg','PrintSjController@datatable_brg')->name('PrintSj.datatable_brg');
        Route::post('datatable_brg_detail','PrintSjController@datatable_brg_detail')->name('PrintSj.datatable_brg_detail');
        Route::post('detele_item', 'PrintSjController@delete_item')->name('PrintSj.delete_item');
        Route::get('nota/{id}', 'PrintSjController@nota')->name('PrintSj.nota');
        Route::get('sj/{id}', 'PrintSjController@sj')->name('PrintSj.sj');
        Route::post('barang_nota', 'PrintSjController@barang_nota')->name('PrintSj.barang_nota');
        Route::post('total', 'PrintSjController@total')->name('PrintSj.total');
        Route::post('add_pelanggan', 'PrintSjController@add_pelanggan')->name('PrintSj.add_pelanggan');
        Route::post('select_pelanggan', 'PrintSjController@select_pelanggan')->name('PrintSj.select_pelanggan');
        Route::get('excel_sj/{tgl}','PrintSjController@excel_sj')->name('PrintSj.excel_sj');
        Route::post('delete_tgl', 'PrintSjController@delete_tgl')->name('PrintSj.delete_tgl');
        Route::post('print_sj_count', 'PrintSjController@print_sj_count')->name('PrintSj.print_sj_count');
    });

    //suplier
    Route::group(['prefix' => 'suplier'], function ()  {
        Route::get('/', 'suplierController@index')->name('suplier.index');
        Route::get('datatable', 'suplierController@datatable')->name('suplier.datatable');
        Route::get('form', 'suplierController@form')->name('suplier.form');
        Route::post('save', 'suplierController@save')->name('suplier.save');
        Route::get('form-edit/{id}', 'suplierController@form_edit')->name('suplier.form_edit');
        Route::post('update', 'suplierController@update')->name('suplier.update');
        Route::post('delete', 'suplierController@delete')->name('suplier.delete');        
    });

    //Bk Veneer Keluar
    Route::group(['prefix' => 'veneerkeluar'], function () {
        Route::get('/', 'BkVeneerKeluarController@index')->name('veneerkeluar.index');
        Route::get('datatable', 'BkVeneerKeluarController@datatable')->name('veneerkeluar.datatable');
        Route::get('form', 'BkVeneerKeluarController@form')->name('veneerkeluar.form');
        Route::post('list-brg', 'BkVeneerKeluarController@list_brg')->name('veneerkeluar_brg.list');     
        Route::post('save', 'BkVeneerKeluarController@save')->name('veneerkeluar.save');
        Route::post('item', 'BkVeneerKeluarController@item_bk')->name('veneerkeluar.item');
        Route::post('delete-item', 'BkVeneerKeluarController@delete_item')->name('veneerkeluar_item.delete');
        Route::post('delete', 'BkVeneerKeluarController@delete')->name('veneerkeluar.delete');
        Route::get('form_editt/{no_bk}', 'BkVeneerKeluarController@form_edit')->name('veneerkeluar.form_edit');
        Route::get('max_no_bk', 'BkVeneerKeluarController@max_no_bk')->name('veneerkeluar.max_no_bk');
        Route::post('total_harga', 'BkVeneerKeluarController@get_total_harga')->name('veneerkeluar_total.json');
        Route::get('print/{no_bk}', 'BkVeneerKeluarController@print')->name('veneerkeluar.print');
        Route::post('add_kepada', 'BkVeneerKeluarController@add_kepada')->name('veneerkeluar.add_kepada');
        Route::post('add_barang', 'BkVeneerKeluarController@add_barang')->name('veneerkeluar.add_barang');
        Route::post('add_satuan', 'BkVeneerKeluarController@add_satuan')->name('veneerkeluar.add_satuan');
        Route::post('get_no_veneerkeluar', 'BkVeneerKeluarController@get_no_veneerkeluar')->name('get_no_veneerkeluar.print');
   
});

  //Barang Masuk Sampah
  Route::group(['prefix' => 'bm-sampah'], function () {
    Route::get('/', 'BmSampahController@index')->name('bmSampah.index');
    Route::get('datatable', 'BmSampahController@datatable')->name('bmSampah.datatable');
    Route::get('datatable_brg', 'BmSampahController@datatable_brg')->name('bmSampah.datatable_brg');
    Route::post('datatable_item', 'BmSampahController@datatable_item')->name('bmSampah.datatable_item');
    Route::get('form', 'BmSampahController@form')->name('bmSampah.form');
    Route::post('save', 'BmSampahController@save')->name('bmSampah.save');
    Route::get('form_edit/{id}', 'BmSampahController@form_edit')->name('bmSampah.form_edit');
    Route::get('delete_bm', 'BmSampahController@delete_bm')->name('bmSampah.delete_bm');     
    Route::get('no_bm', 'BmSampahController@no_bm')->name('bmSampah.no_bm');
    Route::post('add_barang', 'BmSampahController@add_barang')->name('bmSampah.add_barang') ;
    Route::post('delete_item', 'BmSampahController@delete_item')->name('bmSampah.delete_item');
    Route::post('total', 'BmSampahController@total')->name('bmSampah.total');
    Route::get('print/{id}', 'BmSampahController@print')->name('bmSampah.print');
    Route::post('print_bm_count', 'BmSampahController@print_bm_count')->name('bmSampah.print_bm_count');
                
});

//Barang Masuk Gudang Ngelontok
Route::group(['prefix' => 'bm-GdNgelontok'], function () {
    Route::get('/', 'BmGdNgelontokController@index')->name('bmGdNgelontok.index');
    Route::get('datatable', 'BmGdNgelontokController@datatable')->name('bmGdNgelontok.datatable');
    Route::get('datatable_brg', 'BmGdNgelontokController@datatable_brg')->name('bmGdNgelontok.datatable_brg');
    Route::post('datatable_item', 'BmGdNgelontokController@datatable_item')->name('bmGdNgelontok.datatable_item');
    Route::get('form', 'BmGdNgelontokController@form')->name('bmGdNgelontok.form');
    Route::post('save', 'BmGdNgelontokController@save')->name('bmGdNgelontok.save');
    Route::get('form_edit/{id}', 'BmGdNgelontokController@form_edit')->name('bmGdNgelontok.form_edit');
    Route::get('delete_bm', 'BmGdNgelontokController@delete_bm')->name('bmGdNgelontok.delete_bm');     
    Route::get('no_bm', 'BmGdNgelontokController@no_bm')->name('bmGdNgelontok.no_bm');
    Route::post('add_barang', 'BmGdNgelontokController@add_barang')->name('bmGdNgelontok.add_barang') ;
    Route::post('delete_item', 'BmGdNgelontokController@delete_item')->name('bmGdNgelontok.delete_item');
    Route::post('total', 'BmGdNgelontokController@total')->name('bmGdNgelontok.total');
    Route::get('print/{id}', 'BmGdNgelontokController@print')->name('bmGdNgelontok.print');
    Route::post('print_bm_count', 'BmGdNgelontokController@print_bm_count')->name('bmGdNgelontok.print_bm_count');
});

Route::group(['prefix' => 'cek-bm'], function () {
    Route::get('/', 'CekBmController@index')->name('CekBm.index');
    Route::get('datatable', 'CekBmController@datatable')->name('CekBm.datatable');
    Route::post('datatable_bm', 'CekBmController@datatable_bm')->name('CekBm.datatable_bm');
    Route::post('acc_bm', 'CekBmController@acc_bm')->name('CekBm.acc_bm');
    Route::get('print/{id}', 'CekBmController@print')->name('CekBm.print');
    Route::post('print_bm_count', 'CekBmController@print_bm_count')->name('CekBm.print_bm_count');
});

Route::group(['prefix' => 'kas'], function () {
    Route::get('/', 'KasController@index')->name('kas.index');
    Route::get('datatable', 'KasController@datatable')->name('kas.datatable');
    Route::get('form', 'KasController@form')->name('kas.form');
    Route::post('set-awal', 'KasController@set_awal')->name('kas_awal.set');

    Route::get('tgl-kas-get', 'KasController@tgl_kas_get')->name('kas_tgl.get');
    Route::get('tgl-kas-start', 'KasController@tgl_kas_start')->name('kas_tgl.start');
    Route::post('list-kas', 'KasController@list_kas')->name('kas_list.get');
    Route::post('save', 'KasController@save_kas')->name('kas.save');
    Route::post('delete', 'KasController@delete_kas')->name('kas.delete');

    Route::get('show/{tgl}', 'KasController@show_kas')->name('kas.show');
    Route::get('excel/{tgl}', 'KasController@export_excel')->name('kas.excel');
    Route::get('batal-kas', 'KasController@batal_kas')->name('kas.batal');
    Route::get('tutup-kas', 'KasController@tutup_kas')->name('kas.tutup');
});


Route::group(['prefix' => 'perintah'], function () {
    Route::get('/', 'BkPerintahController@index')->name('perintah.index');
    Route::get('datatable', 'BkPerintahController@datatable')->name('perintah.datatable');
    Route::get('form', 'BkPerintahController@form')->name('perintah.form');
    Route::post('list-brg', 'BkPerintahController@list_brg')->name('perintah_brg.list');     
    Route::post('save', 'BkPerintahController@save')->name('perintah.save');
    Route::post('item', 'BkPerintahController@item_bk')->name('perintah.item');
    Route::post('delete-item', 'BkPerintahController@delete_item')->name('perintah_item.delete');
    Route::post('delete', 'BkPerintahController@delete')->name('perintah.delete');
    Route::get('form_editt/{no_bk}', 'BkPerintahController@form_edit')->name('perintah.form_edit');
    Route::get('max_no_bk', 'BkPerintahController@max_no_bk')->name('perintah.max_no_bk');
    Route::post('total_harga', 'BkPerintahController@get_total_harga')->name('perintah_total.json');
    Route::get('print/{no_bk}', 'BkPerintahController@print')->name('perintah.print');
    Route::post('add_kepada', 'BkPerintahController@add_kepada')->name('perintah.add_kepada');
    Route::post('add_barang', 'BkPerintahController@add_barang')->name('perintah.add_barang');
    Route::post('add_satuan', 'BkPerintahController@add_satuan')->name('perintah.add_satuan');
    Route::post('get_no_perintah', 'BkPerintahController@get_no_perintah')->name('get_no_perintah.print');
});

Route::group(['prefix' => 'cek-prsopir'], function () {
    Route::get('/', 'CekPrSopirController@index')->name('cekprsopir.index');
    Route::get('datatable', 'CekPrSopirController@datatable')->name('cekprsopir.datatable');
    Route::post('datatable_bk', 'CekPrSopirController@datatable_bk')->name('cekprsopir.datatable_bk');
    Route::post('acc_bk', 'CekPrSopirController@acc_bk')->name('cekprsopir.acc_bk');
    Route::get('nota/{id}', 'CekPrSopirController@nota')->name('cekprsopir.nota');
    Route::get('bk/{id}', 'CekPrSopirController@bk')->name('cekprsopir.bk');
    Route::get('print/{no_bk}', 'CekPrSopirController@print')->name('cekprsopir.print');
    Route::post('batal', 'CekPrSopirController@batal')->name('cekprsopir.batal');
    Route::post('get-kpd-lh', 'CekPrSopirController@get_kpd_lh')->name('get_kpd_lh.json');
});

Route::group(['prefix' => 'cek-prpenerima'], function () {
    Route::get('/', 'CekPrPenerimaController@index')->name('cekprpenerima.index');
    Route::get('datatable', 'CekPrPenerimaController@datatable')->name('cekprpenerima.datatable');
    Route::post('datatable_bk', 'CekPrPenerimaController@datatable_bk')->name('cekprpenerima.datatable_bk');
    Route::post('acc_bk', 'CekPrPenerimaController@acc_bk')->name('cekprpenerima.acc_bk');
    Route::get('nota/{id}', 'CekPrPenerimaController@nota')->name('cekprpenerima.nota');
    Route::get('bk/{id}', 'CekPrPenerimaController@bk')->name('cekprpenerima.bk');
    Route::get('print/{no_bk}', 'CekPrPenerimaController@print')->name('cekprpenerima.print');
    Route::post('batal', 'CekPrPenerimaController@batal')->name('cekprpenerima.batal');
    Route::post('get-kpd-lh', 'CekPrPenerimaController@get_kpd_lh')->name('get_kpd_lh.json');
});

    // Penjarangan
    Route::group(['prefix' => 'perintah_pj'], function () {
        Route::get('/', 'BkPerintahPjController@index')->name('perintahpj.index');
        Route::get('datatable', 'BkPerintahPjController@datatable')->name('perintahpj.datatable');
        Route::get('form', 'BkPerintahPjController@form')->name('perintahpj.form');
        Route::get('list-brg', 'BkPerintahPjController@list_brg')->name('perintahpj.list');
        // Route::post('get-tembel', 'BkPerintahPjController@get_nama_perintahpj')->name('get_perintahpj.json');
        Route::post('save', 'BkPerintahPjController@save')->name('perintahpj.save');
        Route::post('item', 'BkPerintahPjController@item_bk')->name('perintahpj.item');
        Route::post('delete-item', 'BkPerintahPjController@delete_item')->name('perintahpj_item.delete');
        Route::post('delete', 'BkPerintahPjController@delete')->name('perintahpj.delete');
        Route::get('form_edit/{no_bk}', 'BkPerintahPjController@form_edit')->name('perintahpj.form_edit');
        Route::get('max_no_bk', 'BkPerintahPjController@max_no_bk')->name('perintahpj.max_no_bk');
        Route::post('total_harga', 'BkPerintahPjController@get_total_harga')->name('perintahpj_total_harga.json');
        Route::get('print/{no_bk}', 'BkPerintahPjController@print')->name('perintahpj.print');
        Route::get('list-lahan', 'BkPerintahPjController@list_lahan')->name('perintahpj_lahan.datatable');
        Route::get('list-pekerja', 'BkPerintahPjController@list_pekerja')->name('perintahpj_pekerja.datatable');
        Route::post('get_lahan', 'BkPerintahPjController@get_lahan')->name('perintahpjget_lahan.json');
        Route::post('get_pekerja', 'BkPerintahPjController@get_pekerja')->name('perintahpjget_pekerja.json');
        
        Route::post('pj_lahan', 'BkPerintahPjController@pj_lahan')->name('perintahpj_lahan.delete');
        Route::post('pj_pekerja', 'BkPerintahPjController@pj_pekerja')->name('perintahpj_pekerja.delete');
        Route::post('simpan_lahan', 'BkPerintahPjController@simpan_lahan')->name('perintahpjsimpan_lahan.save');
        Route::post('simpan_pekerja', 'BkPerintahPjController@simpan_pekerja')->name('perintahpjsimpan_pekerja.save');
        Route::post('get_no_penjarangan', 'BkPerintahPjController@get_no_penjarangan')->name('perintahpjget_no_penjarangan.print');
        Route::post('add_pkj', 'BkPerintahPjController@add_pkj')->name('perintahpjpenjarangan.add_pkj');
    });

    Route::group(['prefix' => 'cek-barang'], function () {
        Route::get('/', 'CekBarangController@index')->name('cekbarang.index');
        Route::get('datatable', 'CekBarangController@datatable')->name('cekbarang.datatable');
        Route::post('datatable_bk', 'CekBarangController@datatable_bk')->name('cekbarang.datatable_bk');
        Route::post('acc_bk', 'CekBarangController@acc_bk')->name('cekbarang.acc_bk');
        Route::get('nota/{id}', 'CekBarangController@nota')->name('cekbarang.nota');
        Route::get('bk/{id}', 'CekBarangController@bk')->name('cekbarang.bk');
        Route::get('print/{no_bk}', 'CekBarangController@print')->name('cekbarang.print');
        Route::post('batal', 'CekBarangController@batal')->name('cekbarang.batal');
        Route::post('get-kpd-lh', 'CekBarangController@get_kpd_lh')->name('cekbarang_get_kpd_lh.json');
    });

    Route::group(['prefix' => 'cek-sopir'], function () {
        Route::get('/', 'CekSopirController@index')->name('ceksopir.index');
        Route::get('datatable', 'CekSopirController@datatable')->name('ceksopir.datatable');
        Route::post('datatable_bk', 'CekSopirController@datatable_bk')->name('ceksopir.datatable_bk');
        Route::post('acc_bk', 'CekSopirController@acc_bk')->name('ceksopir.acc_bk');
        Route::get('nota/{id}', 'CekSopirController@nota')->name('ceksopir.nota');
        Route::get('bk/{id}', 'CekSopirController@bk')->name('ceksopir.bk');
        Route::get('print/{no_bk}', 'CekSopirController@print')->name('ceksopir.print');
        Route::post('batal', 'CekSopirController@batal')->name('ceksopir.batal');
        Route::post('get-kpd-lh', 'CekSopirController@get_kpd_lh')->name('ceksopir_get_kpd_lh.json');
    });

    Route::group(['prefix' => 'cek-admin_satu'], function () {
        Route::get('/', 'CekAdminSatuController@index')->name('cekadmin_satu.index');
        Route::get('datatable', 'CekAdminSatuController@datatable')->name('cekadmin_satu.datatable');
        Route::post('datatable_bk', 'CekAdminSatuController@datatable_bk')->name('cekadmin_satu.datatable_bk');
        Route::post('acc_bk', 'CekAdminSatuController@acc_bk')->name('cekadmin_satu.acc_bk');
        Route::get('nota/{id}', 'CekAdminSatuController@nota')->name('cekadmin_satu.nota');
        Route::get('bk/{id}', 'CekAdminSatuController@bk')->name('cekadmin_satu.bk');
        Route::get('print/{no_bk}', 'CekAdminSatuController@print')->name('cekadmin_satu.print');
        Route::post('batal', 'CekAdminSatuController@batal')->name('cekadmin_satu.batal');
        Route::post('get-kpd-lh', 'CekAdminSatuController@get_kpd_lh')->name('cekadmin_satu_get_kpd_lh.json');
    });

    Route::group(['prefix' => 'cek-admin_dua'], function () {
        Route::get('/', 'CekAdminDuaController@index')->name('cekadmin_dua.index');
        Route::get('datatable', 'CekAdminDuaController@datatable')->name('cekadmin_dua.datatable');
        Route::post('datatable_bk', 'CekAdminDuaController@datatable_bk')->name('cekadmin_dua.datatable_bk');
        Route::post('acc_bk', 'CekAdminDuaController@acc_bk')->name('cekadmin_dua.acc_bk');
        Route::get('nota/{id}', 'CekAdminDuaController@nota')->name('cekadmin_dua.nota');
        Route::get('bk/{id}', 'CekAdminDuaController@bk')->name('cekadmin_dua.bk');
        Route::get('print/{no_bk}', 'CekAdminDuaController@print')->name('cekadmin_dua.print');
        Route::post('batal', 'CekAdminDuaController@batal')->name('cekadmin_dua.batal');
        Route::post('get-kpd-lh', 'CekAdminDuaController@get_kpd_lh')->name('cekadmin_dua_get_kpd_lh.json');
    });



        // // bk sampah_banding
        // Route::group(['prefix' => 'bk_banding'], function () {
        //     Route::get('/', 'BkControllerbanding@index')->name('bk_banding.index');
        //     Route::get('datatable', 'BkControllerbanding@datatable')->name('bk_banding.datatable');
        //     Route::get('form', 'BkControllerbanding@form')->name('bk_banding.form');
        //     Route::get('list-brg', 'BkControllerbanding@list_brg')->name('bk_banding_brg.list');
        //     Route::post('save', 'BkControllerbanding@save')->name('bk_banding.save');
        //     Route::post('item', 'BkControllerbanding@item_bk')->name('bk_banding.item');
        //     Route::post('delete-item', 'BkControllerbanding@delete_item')->name('bk_banding_item.delete');
        //     Route::post('delete', 'BkControllerbanding@delete')->name('bk_banding.delete');
        //     Route::get('form_editt/{no_bk}', 'BkControllerbanding@form_edit')->name('bk_banding.form_edit');
        //     Route::get('max_no_bk', 'BkControllerbanding@max_no_bk')->name('bk_banding.max_no_bk');
        //     Route::post('total_harga', 'BkControllerbanding@get_total_harga')->name('total_harga_banding.json');
        //     Route::get('print/{no_bk}', 'BkControllerbanding@print')->name('bk_banding.print');
        //     Route::post('get-no', 'BkControllerbanding@get_no')->name('get_no_banding.print');

        // });

        // Route::group(['prefix' => 'banding'], function () {
        // Route::get('/', 'BkControllerpembanding@index')->name('banding.index');
        // Route::get('detail', 'BkControllerpembanding@detail')->name('banding.detail');
        // Route::get('datatable', 'BkControllerpembanding@datatable')->name('banding.datatable');

        // });

          //Barang Masuk Sampah
  Route::group(['prefix' => 'bm-sampah'], function () {
    Route::get('/', 'BmSampahController@index')->name('bmSampah.index');
    Route::get('datatable', 'BmSampahController@datatable')->name('bmSampah.datatable');
    Route::get('datatable_brg', 'BmSampahController@datatable_brg')->name('bmSampah.datatable_brg');
    Route::post('datatable_item', 'BmSampahController@datatable_item')->name('bmSampah.datatable_item');
    Route::get('form', 'BmSampahController@form')->name('bmSampah.form');
    Route::post('save', 'BmSampahController@save')->name('bmSampah.save');
    Route::get('form_edit/{id}', 'BmSampahController@form_edit')->name('bmSampah.form_edit');
    Route::get('delete_bm', 'BmSampahController@delete_bm')->name('bmSampah.delete_bm');     
    Route::get('no_bm', 'BmSampahController@no_bm')->name('bmSampah.no_bm');
    Route::post('add_barang', 'BmSampahController@add_barang')->name('bmSampah.add_barang') ;
    Route::post('delete_item', 'BmSampahController@delete_item')->name('bmSampah.delete_item');
    Route::post('total', 'BmSampahController@total')->name('bmSampah.total');
    Route::get('print/{id}', 'BmSampahController@print')->name('bmSampah.print');
    Route::post('print_bm_count', 'BmSampahController@print_bm_count')->name('bmSampah.print_bm_count');
                
});

//Barang Masuk Gudang Ngelontok
Route::group(['prefix' => 'bm-GdNgelontok'], function () {
    Route::get('/', 'BmGdNgelontokController@index')->name('bmGdNgelontok.index');
    Route::get('datatable', 'BmGdNgelontokController@datatable')->name('bmGdNgelontok.datatable');
    Route::get('datatable_brg', 'BmGdNgelontokController@datatable_brg')->name('bmGdNgelontok.datatable_brg');
    Route::post('datatable_item', 'BmGdNgelontokController@datatable_item')->name('bmGdNgelontok.datatable_item');
    Route::get('form', 'BmGdNgelontokController@form')->name('bmGdNgelontok.form');
    Route::post('save', 'BmGdNgelontokController@save')->name('bmGdNgelontok.save');
    Route::get('form_edit/{id}', 'BmGdNgelontokController@form_edit')->name('bmGdNgelontok.form_edit');
    Route::get('delete_bm', 'BmGdNgelontokController@delete_bm')->name('bmGdNgelontok.delete_bm');     
    Route::get('no_bm', 'BmGdNgelontokController@no_bm')->name('bmGdNgelontok.no_bm');
    Route::post('add_barang', 'BmGdNgelontokController@add_barang')->name('bmGdNgelontok.add_barang') ;
    Route::post('delete_item', 'BmGdNgelontokController@delete_item')->name('bmGdNgelontok.delete_item');
    Route::post('total', 'BmGdNgelontokController@total')->name('bmGdNgelontok.total');
    Route::get('print/{id}', 'BmGdNgelontokController@print')->name('bmGdNgelontok.print');
    Route::post('print_bm_count', 'BmGdNgelontokController@print_bm_count')->name('bmGdNgelontok.print_bm_count');
});

// CEK BM
Route::group(['prefix' => 'cek-bm'], function () {
    Route::get('/', 'CekBmController@index')->name('CekBm.index');
    Route::get('datatable', 'CekBmController@datatable')->name('CekBm.datatable');
    Route::post('datatable_bm', 'CekBmController@datatable_bm')->name('CekBm.datatable_bm');
    Route::post('acc_bm', 'CekBmController@acc_bm')->name('CekBm.acc_bm');
    Route::get('print/{id}', 'CekBmController@print')->name('CekBm.print');
    Route::post('print_bm_count', 'CekBmController@print_bm_count')->name('CekBm.print_bm_count');
});

// Rekap BM
Route::group(['prefix' => 'rekapBm'], function () {
    Route::get('/', 'RekapBmController@index')->name('RekapBm.index');
    Route::get('excel_bm/{tgl}','RekapBmController@excel_bm')->name('RekapBm.excel_bm');
    Route::post('delete_tgl', 'RekapBmController@delete_tgl')->name('RekapBm.delete_tgl');
});

// BM Veneer
Route::group(['prefix' => 'bmVeneer'], function () {
    Route::get('/', 'BmVeneerController@index')->name('bmVeneer.index');
    Route::get('datatable', 'BmVeneerController@datatable')->name('bmVeneer.datatable');
    Route::get('datatable_brg', 'BmVeneerController@datatable_brg')->name('bmVeneer.datatable_brg');
    Route::get('datatable_veneer', 'BmVeneerController@datatable_veneer')->name('bmVeneer.datatable_veneer');
    Route::post('datatable_item', 'BmVeneerController@datatable_item')->name('bmVeneer.datatable_item');
    Route::get('form', 'BmVeneerController@form')->name('bmVeneer.form');
    Route::post('save', 'BmVeneerController@save')->name('bmVeneer.save');
    Route::get('form_edit/{id}', 'BmVeneerController@form_edit')->name('bmVeneer.form_edit');
    Route::get('delete_bm', 'BmVeneerController@delete_bm')->name('bmVeneer.delete_bm');     
    Route::get('no_bm', 'BmVeneerController@no_bm')->name('bmVeneer.no_bm');
    Route::post('add_barang', 'BmVeneerController@add_barang')->name('bmVeneer.add_barang') ;
    Route::post('delete_item', 'BmVeneerController@delete_item')->name('bmVeneer.delete_item');
    Route::post('total', 'BmVeneerController@total')->name('bmVeneer.total');
    Route::get('print/{id}', 'BmVeneerController@print')->name('bmVeneer.print');
    Route::post('print_bm_count', 'BmVeneerController@print_bm_count')->name('bmVeneer.print_bm_count');
    Route::post('get_veneer', 'BmVeneerController@get_nama_veneer')->name('get_veneer.json');
});

// BM dari BK
Route::group(['prefix' => 'bmDariBk'], function () {
    Route::get('/', 'BmDariBkController@index')->name('bmDariBk.index');
    Route::get('listBk', 'BmDariBkController@listBk')->name('bmDariBk.listBk');
    Route::get('listBk-datatable', 'BmDariBkController@listBk_datatable')->name('list_bk.datatable');
    Route::post('item-listBk-datatable', 'BmDariBkController@item_listBk_datatable')->name('item_list_bk.datatable');
    Route::post('buat-bm','BmDariBkController@buat_bm')->name('list_bk.buat_bm');
    Route::post('total', 'BmDariBkController@total')->name('list_bk.total');

    Route::get('datatable', 'BmDariBkController@datatable')->name('bmDariBk.datatable');
    Route::get('datatable_brg', 'BmDariBkController@datatable_brg')->name('bmDariBk.datatable_brg');
    Route::post('datatable_bmDariBk', 'BmDariBkController@datatable_bmDariBk')->name('bmDariBk.datatable_bmDariBk');
    Route::post('datatable_item', 'BmDariBkController@datatable_item')->name('bmDariBk.datatable_item');
    Route::get('form', 'BmDariBkController@form')->name('bmDariBk.form');
    Route::post('save', 'BmDariBkController@save')->name('bmDariBk.save');
    Route::get('form_edit/{id}', 'BmDariBkController@form_edit')->name('bmDariBk.form_edit');
    Route::get('delete_bm', 'BmDariBkController@delete_bm')->name('bmDariBk.delete_bm');     
    Route::get('no_bm', 'BmDariBkController@no_bm')->name('bmDariBk.no_bm');
    Route::post('add_barang', 'BmDariBkController@add_barang')->name('bmDariBk.add_barang') ;
    Route::post('delete_item', 'BmDariBkController@delete_item')->name('bmDariBk.delete_item');
    // Route::post('total', 'BmDariBkController@total')->name('bmDariBk.total');
    Route::get('print/{id}', 'BmDariBkController@print')->name('bmDariBk.print');
    Route::post('print_bm_count', 'BmDariBkController@print_bm_count')->name('bmDariBk.print_bm_count');
    Route::post('get_veneer', 'BmDariBkController@get_nama_veneer')->name('get_veneer.json');
});


































// Route::group(['prefix' => 'suplier'], function () {
    //     Route::get('/', 'SuplierController@index')->name('suplier.index');
    //     Route::get('datatable', 'SuplierController@datatable')->name('suplier.datatable');
    //     Route::get('form', 'SuplierController@form')->name('suplier.form');
    //     Route::post('save', 'SuplierController@save')->name('suplier.save');
    //     // Route::get('delete/{id}', 'AdminController@delete_shift')->name('suplier.delete');
    // });
    
    // Route::group(['prefix' => 'permintaan'], function () {
        //     Route::get('/', 'PermintaanController@index')->name('permintaan.index');
        //     Route::get('datatable', 'PermintaanController@datatable')->name('permintaan.datatable');
        //     Route::get('form', 'PermintaanController@form')->name('permintaan.form');
        //     Route::post('save', 'PermintaanController@save')->name('permintaan.save');
        //     Route::post('detail', 'PermintaanController@permintaan_barang')->name('permintaan.detail');
        //     Route::post('delete-brg', 'PermintaanController@delete_barang')->name('permintaan_brg.delete');
        // });
        // Route::get('/welcome', function () {
            //     return view('welcome');
            // });        
            // Route::get('/', 'HomeController@index')->name('dashboard.index');
});
