<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
// use Excel;
// use App\User;
// use App\Exports\UserExport;
// use App\Exports\PresensiExport;

class HomeController extends Controller
{
    private $apina;
    private $barangQ;
    private $sjQ;
    private $url;

    public function __construct()
    {
        date_default_timezone_set("Asia/Jakarta"); 
        $this->url = 'http://adm.wijayaplywoodsindonesia.com/api/ina/stok';
        // $this->url = 'http://192.168.5.9:8080/api/ina/stok';
        $this->apina = json_decode(file_get_contents($this->url), true);
        $this->barangQ = DB::table('barang')->get();
        $this->sjQ = DB::table('suratjalan_detail as a')
                        ->leftJoin('suratjalan as b', 'b.id', '=', 'a.id_sj')
                        ->whereNotNull('b.bayar')
                        ->whereNotNull('b.is_cek_nota')
                        ->whereNull('b.is_batal')
                        ->select('b.id as id_sj','b.tgl','a.nama_brg','a.qty','a.harga','a.harga_new')
                        ->get();
    }

    public function index()
    {
        $sum_penjualan = DB::table('suratjalan')
                                ->where('tgl', date('Y-m-d'))
                                ->whereNotNull('bayar')
                                ->whereNotNull('is_cek_nota')
                                ->whereNull('is_batal')
                                ->sum('total');
                                
        $count_pelanggan = DB::table('suratjalan')
                                ->where('tgl', date('Y-m-d'))
                                ->whereNotNull('bayar')
                                ->whereNotNull('is_cek_nota')
                                ->whereNull('is_batal')
                                ->count('id');
        
        $sum_pembelian = DB::table('beli')
                                ->where('tgl', date('Y-m-d'))
                                ->whereNotNull('is_cek_beli')
                                ->sum('total');                        

        $data['total_penjualan'] = number_format($sum_penjualan,0,',','.');
        $data['total_pelanggan'] = $count_pelanggan;
        $data['total_pembelian'] = number_format($sum_pembelian,0,',','.');
        return view('admin.dashboard.index')->with($data);
    }

    public function cari_bulan($data, $bulan)
    {
        $dty = array_filter($data->toArray(), function ($value) use ($bulan){
            return $value->bulan == $bulan;
        });

        $sy = 0;
        foreach ($dty as $x) {
            $sy = $x->total;
        }

        return $sy;
    }

    public function grafik(Request $req)
    {

        $tgl = $req->_tgl;
        $data = DB::table('suratjalan')
                        ->select(DB::raw('sum(total) as total'),  DB::raw('YEAR(tgl) tahun, MONTH(tgl) bulan'))
                        ->whereYear('tgl', $tgl)
                        ->groupby('tahun','bulan')
                        ->orderBy('tgl', 'ASC')
                        ->get();
        $bulan = [
                    "Januari",
                    "Februari",
                    "Maret",
                    "April",
                    "Mei",
                    "Juni",
                    "Juli",
                    "Agustus",
                    "September",
                    "Oktober",
                    "November",
                    "Desember"
                ];

        foreach ($bulan as $key => $v) {
            $no = $key+1;
            $dt[] = [
                        "no" => $no,
                        "bulan" => $v,
                        "total" => $this->cari_bulan($data,$no)
            ];
        }

        return response()->json($dt);
    }
}