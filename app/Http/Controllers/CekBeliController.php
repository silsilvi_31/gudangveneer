<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;

class CekBeliController extends Controller
{
    private $retur_brg;
    public function __construct()
    {
        date_default_timezone_set("Asia/Jakarta");
        $this->retur_brg = DB::table('retur_brg')->where('jenis', 'beli')->get();
    }

    public function index()
    {
        return view('admin.CekBeli.index');
    }

    public function get_karyawan($id_user)
    {
        $data = DB::table('karyawan')->where('id_users', $id_user)->first();
        return $data->nama;
    }

    public function get_total_retur($data, $id_sj)
    {
        $dty = array_filter($data->toArray(), function ($value) use ($id_sj) {
            return $value->id_sj == $id_sj;
        });

        $grandtotal = 0;
        foreach ($dty as $v) {
            $grandtotal += $v->subtotal;
        }

        return $grandtotal;
    }

    public function datatable()
    {
        $parent_jurnal = DB::table('parent_jurnal')
                                ->where('status', 'tutup')
                                ->orderBy('created_at', 'DESC')
                                ->first();

        $tgl_akhir = isset($parent_jurnal->tgl_akhir) ? $parent_jurnal->tgl_akhir : '';                                

        $beli = DB::table('beli')
                            ->whereDate('tgl', '<', $tgl_akhir)
                            ->get();
        
        $id_beli = [];

        foreach ($beli as $value) {
            $id_beli[] = $value->id_beli;
        }

        $data = DB::table('beli as a')
                            ->leftJoin('suplier as b', 'a.suplier', '=', 'b.id')
                            ->whereNotIn('id_beli', $id_beli)
                            ->select('a.id_beli', 'a.tgl', 'b.nama', 'a.total', 'a.form', 'a.no_nota_spl',
                                        'a.is_cek_beli', 'a.cek_beli', 'a.user_batal', 'a.ket_batal', 'a.user_add')
                            ->orderBy('a.id_beli', 'DESC')
                            ->get();

        $retur_brg = $this->retur_brg;

        return Datatables::of($data)
        ->editColumn('is_cek_beli', function ($data) {
            $is_cek_beli = $data->is_cek_beli;
            $user_add = isset($data->user_add) ? $this->get_karyawan($data->user_add) : NULL;
            $cek_beli = isset($is_cek_beli) ? $this->get_karyawan($data->cek_beli) : NULL;
            $is_batal = isset($data->user_batal) ? $this->get_karyawan($data->user_batal).' ('.$data->ket_batal.')' : NULL;

            $status_user_add = isset($user_add) ? NULL : 'hidden';
            $status_cek_beli = isset($cek_beli) ? NULL : 'hidden';
            $status_batal = isset($is_batal) ? NULL : 'hidden';

            $cek = 'Input : <span class="badge badge-primary" '.$status_user_add.'> '.$user_add.' </span> </br>
                    Cek : <span class="badge badge-success" '.$status_cek_beli.'> '.$cek_beli.' </span> </br>
                    Batal : <span class="badge badge-danger" '.$status_batal.'> '.$is_batal.' </span> </br>';
            return $cek;
        }) 
        ->addColumn('opsi', function ($data) use ($retur_brg){
            $id_beli = $data->id_beli;
            $tgl = date("d-m-Y", strtotime($data->tgl));
            $suplier = $data->nama;
            $form = $data->form;
            $total = $data->total;
            $no_nota =  $data->no_nota_spl;
            $grandtotal_retur = $this->get_total_retur($retur_brg, $data->id_beli);
            $grandtotal = $total - $grandtotal_retur;
            $cek_beli = ($data->is_cek_beli == 1) ? 'acc' : 'tidak acc';
            $status_batal = ( ( ($data->is_cek_beli != 1)  && ($grandtotal_retur > 0) ) OR
                            ( ($data->is_cek_beli != 1)  || ($grandtotal_retur > 0) ) ) ? 'disabled' : '' ;
            $batal = $data->ket_batal;
            return '<button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal_cekbeli" data-form="cek_beli" data-id="'.$id_beli.'" data-tgl="'.$tgl.'" data-suplier="'.$suplier.'" data-nota="'.$no_nota.'" data-total="'.$total.'" data-totalretur="'.$grandtotal_retur.'" data-grandtotal="'.$grandtotal.'" data-cek="'.$cek_beli.'">Cek Beli</button>
                    <button type="button" class="btn btn-sm btn-danger" '.$status_batal.' data-toggle="modal" data-target="#modal_btl" data-id="'.$id_beli.'" data-ket="'.$batal.'">Batal</button>';
        })
        ->rawColumns(['is_cek_beli', 'opsi'])
        ->make(true);
    }

    public function datatable_beli(Request $req)
    {
        $id = $req->_id;
        $data = DB::table('beli as a')
                    ->where('a.status', NULL)
                    ->where('a.id_beli', $id)
                    ->leftJoin('beli_detail as b', 'a.id_beli', '=', 'b.id_detail_beli')
                    ->leftJoin('retur_brg as r', 'r.id_detail', '=', 'b.id')
                    ->leftJoin('satuan as c', 'b.id_satuan', '=', 'c.id')
                    // ->where('r.jenis', 'beli')
                    ->select('a.id','a.id_beli','b.nama_brg','b.ketr','b.qty','b.harga','b.subtotal',
                                'c.nama as satuan', 'r.qty_retur')
                    ->get();

        return Datatables::of($data)  
        ->addIndexColumn() 
        ->make(true);                                        
    }

    public function acc_beli(Request $req)
    {
        $id_users = session::get('id_user');
        $id = $req->_id;
        $btn = $req->_btn;
        $status = $req->_status;
        
        $data_acc_beli = [
            'is_cek_beli' => 1,
            'cek_beli' => $id_users,
            'user_upd' => $id_users,
            'updated_at' => date("Y-m-d H:i:s")
        ];

        $data_batal_beli = [
            'is_cek_beli' => 2,
            'cek_beli' => $id_users,
            'user_upd' => $id_users,
            'updated_at' => date("Y-m-d H:i:s")
        ];
        
        if($status != 'acc') {
            if ($btn == 'setuju') {
                $update_beli = DB::table('beli')
                                ->where('id_beli', $id)
                                ->update($data_acc_beli);
                                
                if ($update_beli) {
                    $res = [
                        'code' => 201,
                        'msg' => 'beli Berhasil Di ACC'
                    ];
                } else {
                    $res = [
                        'code' => 400,
                        'msg' => 'beli gagal di ACC'
                    ];
                }
            } else if ($btn == 'batal') {
                $update_beli = DB::table('beli')
                            ->where('id_beli', $id)
                            ->update($data_batal_beli);
                            
                if ($update_beli) {
                    $res = [
                        'code' => 300,
                        'msg' => 'beli Batal di Acc'
                    ];
                } else {
                    $res = [
                        'code' => 400,
                        'msg' => 'beli gagal untuk dibatalkan'
                    ];
                }
            }
        } else {
            $res = [
                'code' => 400,
                'msg' => 'beli sudah di ACC'
            ];
        }
        return response()->json($res);
    }

    public function get_ket($id_beli)
    {
        $detail_beli = DB::table('beli_detail as a')
                                ->leftJoin('satuan as b', 'b.id', '=', 'a.id_satuan')
                                ->where('id_detail_beli', $id_beli)->get();

        $dt = [];
        foreach ($detail_beli as $value) {
            $dt[] = $value->nama_brg.' @'.$value->nama.' '.$value->ketr;
        }

        $hasil = implode(', ', $dt);
        return $hasil;
    }

    public function set_akun_batal($beli, $detail_beli, $ket_batal)
    {
        $k = 0;

        $data = DB::table('beli_detail')
                        ->where('id_detail_beli', $beli['id_beli'])
                        ->select(DB::raw('sum(qty) as qty'), DB::raw('sum(subtotal) as total'))
                        ->first();

        $akun[$k]['tgl'] = date('Y-m-d');
        $akun[$k]['id_item'] = null;
        $akun[$k]['no_akun'] = $beli['no_akun'];
        $akun[$k]['jenis_jurnal'] = 'beli-batal';
        $akun[$k]['ref'] = $beli['no_nota_spl'];
        $akun[$k]['bm'] = $beli['id_beli'];
        $akun[$k]['nama'] = $beli['member'];
        $akun[$k]['keterangan'] = $this->get_ket($beli['id_beli']).' // '.$ket_batal;
        $akun[$k]['map'] = 'd';
        $akun[$k]['hit'] = null;
        $akun[$k]['grup'] = 1;
        $akun[$k]['qty'] = $data->qty;
        $akun[$k]['m3'] = null;
        $akun[$k]['harga'] = $data->total;
        $akun[$k]['total'] = $data->total;

        $kk = $k+1;
        foreach ($detail_beli as $key => $value) {
            $kk++;
            $akun[$kk]['tgl'] = date('Y-m-d');
            $akun[$kk]['id_item'] = $value['id_item'];
            $akun[$kk]['no_akun'] = $value['no_akun_brg'];
            $akun[$kk]['jenis_jurnal'] = 'beli-batal';
            $akun[$kk]['ref'] = $beli['no_nota_spl'];
            $akun[$kk]['bm'] = $beli['id_beli'];
            $akun[$kk]['nama'] = $beli['member'];
            $akun[$kk]['keterangan'] = $value['nama_brg'].' // '.$ket_batal;
            $akun[$kk]['map'] = 'k';
            $akun[$kk]['hit'] = 'b';
            $akun[$kk]['grup'] = 2;
            $akun[$kk]['qty'] = $value['qty'];
            $akun[$kk]['m3'] = null;
            $akun[$kk]['harga'] = $value['harga'];
            $akun[$kk]['total'] = $value['qty'] * $value['harga'];
        }

        $insert = DB::table('jurnal')->insert($akun);
    }

    public function batal_beli(Request $req)
    {
        $id_users = session::get('id_user');
        $id = $req->_id;
        $ket = $req->_ket;
        
        $data_btl_beli = [
            'user_batal' => $id_users,
            'ket_batal' => $ket,
            'user_upd' => $id_users,
            'updated_at' => date("Y-m-d H:i:s")
        ];

        $cek_batal = DB::table('beli')->where('id_beli', $id)->whereNotNull('user_batal')->first();
        
        if (isset($cek_batal)) {
            $res = [
                'code' => 400,
                'msg' => 'Data sudah pernah dibatalkan'
            ];
        } elseif (!isset($ket)){
            $res = [
                'code' => 400,
                'msg' => 'Isi Keterangan Pembatalan'
            ];
        } else {
            //awal jurnal balik Pembelian
            
            $beli = DB::table('beli as a')
                        ->leftJoin('suplier as b', 'a.suplier', '=', 'b.id')
                        ->select('a.id_beli as id_beli', 'a.tgl', 'b.nama', 'a.no_akun', 'a.no_nota_spl')
                        ->where('a.id_beli', $id)
                        ->first();
            
            $beli_detail = DB::table('beli_detail as a')
                        ->leftJoin('barang as b', 'a.id_brg', '=', 'b.kode')
                        ->select('a.id as id_item', 'a.id_detail_beli', 'a.id_brg', 'a.nama_brg', 'a.harga', 'a.qty', 'b.hpp', 'a.no_akun_brg')
                        ->where('a.id_detail_beli', $id)
                        ->get();

            $dt_beli = [
                'id_beli'       => $beli->id_beli,
                'tgl'           => $beli->tgl,
                'member'        => $beli->nama,
                'no_akun'       => $beli->no_akun,
                'no_nota_spl'   => $beli->no_nota_spl
            ];

            $dt = [];
            foreach ($beli_detail as $key => $v) {
                $dt[] = [
                    'id_item'       => $v->id_item,
                    'id_detail_beli'=> $v->id_detail_beli,
                    'id_brg'        => $v->id_brg,
                    'nama_brg'      => $v->nama_brg,
                    'harga'         => $v->harga,
                    'hpp'           => $v->hpp,
                    'qty'           => $v->qty,
                    'no_akun_brg'   => $v->no_akun_brg
                ];
            }

            //akhir jurnal balik SJ

            $update = DB::table('beli')->where('id_beli', $id)->update($data_btl_beli);

            if ($update) {
                $this->set_akun_batal($dt_beli, $dt, $ket);
                $res = [
                    'code' => 200,
                    'msg' => 'Data berhasil dibatalkan'
                ];
            } else {
                $res = [
                    'code' => 400,
                    'msg' => 'Data gagal dibatalkan'
                ];
            }
        }
        return response()->json($res);
    }
}
