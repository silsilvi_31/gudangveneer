<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;

class SatuanController extends Controller
{
    public function __construct ()
    {
        date_default_timezone_set("Asia/Jakarta");    
    }

    public function index()
    {
        return view('admin.master.satuan.index');
    }

    public function datatable()
    {
        $data = DB::table('satuan')->where('status', NULL)->get();
        return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data) {
            $edit = route('satuan.form_edit', [base64_encode($data->id)]);
            $id_satuan = "'".base64_encode($data->id)."'";
            return '<a href="'.$edit.'" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                    <button type="button" class="btn btn-sm btn-danger" onclick="delete_satuan('.$id_satuan.')"><i class="fa fa-trash"></i></button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
        // dd($data);
    }

    public function form()
    {
        return view('admin.master.satuan.form');
    }

    public function save(Request $req)
    {
        $id_user = session::get('id_user');
        $satuan = $req->satuan;
        $data_satuan = [
                        "nama" => $satuan,
                        "created_at" => date("Y-m-d H:i:s"),
                        "user_add" => $id_user
        ];
        $insert_satuan = DB::table('satuan')->insert($data_satuan);
        if ($insert_satuan) {
            $res = [
                'code' => 200,
                'msg' => 'Berhasil disimpan'
            ];
        } else {
            $res = [
            'code' => 400,
            'msg' => 'Gagal disimpan'
            ];
        }

        $data['response'] = $res;
        // dd($data_satuan);
        return redirect()->route('satuan.index')->with($data);
    }

    public function form_edit($id)
    {
        $id_satuan = base64_decode($id);
        $satuan = DB::table('satuan')->where('id',$id_satuan)->first();

        $data['id_satuan'] = base64_encode($satuan->id);
        $data['satuan'] = $satuan->nama;

        return view('admin.master.satuan.form_edit')->with($data);
    }

    public function update(Request $req)
    {
        $id_user = session::get('id_user');
        $id_satuan = base64_decode($req->id_satuan);
        $satuan = $req->satuan;

        $data_satuan = [
            'nama' => $satuan,
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user
        ];

        $update = DB::table('satuan')->where('id', $id_satuan)->update($data_satuan);
        if ($update) {
            $res = [
                'code' => 201,
                'msg' => 'Data telah diupdate'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal diupdate'
            ];
        }
        $data['response'] = $res;
        return redirect()->route('satuan.index')->with($data);
    }

    public function delete(Request $req)
    {
        $id_user = session::get('id_user');
        $id_satuan = base64_decode($req->_idSatuan);
 
        $data_satuan = [
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user,
            'status' => 9
        ];
        $res = [];
        $update = DB::table('satuan')->where('id',$id_satuan)->update($data_satuan);
        if ($update) {
            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    
    }
}
