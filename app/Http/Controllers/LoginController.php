<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use Hash;

class LoginController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function post_login(Request $req)
    {
        $username = $req->username;
        $password = $req->password;
        
        $users = DB::table('users')->where('username', $username)->where('status', 1)->first();
        if (!is_null($users) && (Hash::check($password, $users->password))) {
            Session::put('id_user',$users->id);
            Session::put('username',$username);
            Session::put('password',$password);
            Session::put('role',$users->role);
            Session::put('akses',$users->akses);

            if ($users->role == 1) {
                Session::put('nama', 'Pimpinan');                
            }elseif ($users->role == 2) {
                $karyawan = DB::table('karyawan')->where('id_users', $users->id)->first();
                Session::put('nama', $karyawan->nama);
            }
            return redirect()->route('sjdefault.form');
        }else {
            return 'Username & Password tidak sesuai';
        }
    }

    public function logout()
    {
        Session::flush();
        return redirect()->route('login');
    }

    public function add_user()
    {
        $id_users = uniqid();
        
        $users = [
            'id' => $id_users,
            'username' => '6120', 
            'password' => bcrypt('chichil123'),
            'created_at' => date("Y-m-d H:i:s"),
            'user_add' => '99999',
            'status' => 1
        ];

        $res = [];
        $insert = DB::table('users')->insert($users);
        if ($insert) {
            $res = [
                'msg' => 'save'
            ];    
        }else {
            $res = [
                'msg' => 'save'
            ];
        }

        return response()->json($res);
    }
}