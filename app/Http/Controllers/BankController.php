<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;

class BankController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        return view('admin.master.bank.index');
    }

    public function datatable()
    {
        $data = DB::table('bank')->get();

        return datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data){
            $edit = route('bank.form_edit', [base64_encode($data->id)]);
            $id_bank = "'".base64_encode($data->id)."'";
            return '<a href="'.$edit.'" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                    <button type="button" class="btn btn-sm btn-danger" onclick="delete_bank('.$id_bank.')"><i class="fa fa-trash"></i></button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function form()
    {
        return view('admin.master.bank.form');
    }

    public function save(Request $req)
    {
        $id_user = session::get('id_user');
        $no_rek = $req->no_rek;
        $nama = $req->nama;

        $data_bank = [
                        "no_rek" => $no_rek,
                        "nama" => $nama
        ];

        $insert_bank = DB::table('bank')->insert($data_bank);
        if ($insert_bank) {
            $res = [
                'code' => 300,
                'msg' => 'Berhasil Disimpan'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal Disimpan'
            ];
        }

        $data['response'] = $res;
        return redirect()->route('bank.index')->with($data);
    }

    public function form_edit($id)
    {
        $id_bank = base64_decode($id);
        $bank = DB::table('bank')->where('id', $id_bank)->first();

        $data['id_bank'] = $bank->id;
        $data['no_rek'] = $bank->no_rek;
        $data['nama'] = $bank->nama;

        return view('admin.master.bank.form_edit')->with($data);
    }  
    
    public function update(Request $req)
    {
        $id_user = session::get('id_user');
        $id_bank = $req->id_bank;
        $no_rek = $req->no_rek;
        $nama = $req->nama;

        $data_bank = [
                        'no_rek' => $no_rek,
                        'nama' => $nama
                    ]; 
                    
        $update = DB::table('bank')->where('id', $id_bank)->update($data_bank);
        if ($update) {
            $res = [
                'code' => 201,
                'msg' => 'Data telah diupdate'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal diupdate'
            ];
        }
        $data['response'] = $res;
        return redirect()->route('bank.index')->with($data);
    }

    public function delete(Request $req) 
    {
        $id_user = session::get('id_user');
        $id_bank = base64_decode($req->_idBank);
        
        $res = [];
        $delete = DB::table('bank')->where('id', $id_bank)->delete();
        if($delete) {
            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }
}
