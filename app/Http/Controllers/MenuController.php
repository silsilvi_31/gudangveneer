<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MainMenu;
use Yajra\Datatables\Datatables;
use DB;

class MenuController extends Controller
{
    public function index()
    {
        // $menu = MainMenu::whereNull('parent_id')
        //         ->with('child_menu')
        //         ->get();
        // dd($menu);
        return view('setting.menu.index');
    }

    public function datatable()
    {
        $data = DB::table('menu as a')
                ->leftJoin('menu as b', 'a.parent_id', '=', 'b.id')
                ->select('a.id', 'a.nama', 'a.icon', 'a.route', 'b.nama as parent')
                ->get();

        return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data) {
            $id = $data->id;
            $edit = route('menu.edit', [$id]);
            return '<a href="'.$edit.'" class="btn btn-sm btn-info"><i class="fa fa-fw fa-edit"></i></a>
                    <button class="btn btn-sm btn-danger" onclick="delete_fitur('.$id.')"><i class="fa fa-fw fa-trash"></i></button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function form()
    {
        return view('setting.menu.form');
    }

    public function save(Request $req)
    {
        $id = $req->id;
        $fitur = $req->fitur;
        $icon = $req->icon;
        $route = $req->route;
        $parent_id = $req->parent_id;

        $menu = [
            'nama' => $fitur,
            'icon' => $icon,
            'route' => $route,
            'parent_id' => $parent_id
        ];

        $res = [];
        
        if (isset($id)) {
            $update = DB::table('menu')->where('id', $id)->update($menu);
            if ($update) {
                $res = [
                    'code' => 201,
                    'msg' => 'Berhasil diupdate'
                ];
            }
            $data['response'] = $res;
            return redirect()->back()->with($data);
        }else {
            $insert = DB::table('menu')->insert($menu);
            if ($insert) {
                $res = [
                    'code' => 200,
                    'msg' => 'Berhasil disimpan'
                ];
            }else {
                $res = [
                    'code' => 400,
                    'msg' => 'Gagal disimpan'
                ];
            }

            $data['response'] = $res;
            return redirect()->route('menu.index')->with($data);
        }
    }

    public function edit($id)
    {
        $menu = DB::table('menu')->where('id', $id)->first();
        $data['id'] = $menu->id;
        $data['nama'] = $menu->nama;
        $data['icon'] = $menu->icon;
        $data['route'] = $menu->route;
        $data['parent_id'] = $menu->parent_id;

        return view('setting.menu.form')->with($data);
    }

    public function delete(Request $req)
    {
        $delete = DB::table('menu')->where('id', $req->_id)->delete();

        if ($delete) {
            $res = [
                'code' => 500,
                'msg' => 'Berhasil dihapus !'
            ];
        }else{
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus !'
            ];
        }

        $data['response'] = $res;
        return response()->json($data);
    }

    public function akses()
    {
        return view('setting.akses.index');
    }

    public function datatable_akses()
    {
        $akses = DB::table('akses')
                    ->get();

        return Datatables::of($akses)
        ->addIndexColumn()
        ->addColumn('fitur', function ($akses) {
            return $this->column_fitur($akses->id);
        })
        ->addColumn('opsi', function ($akses) {
            $id = $akses->id;
            return '<a href="#" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                    <button class="btn btn-sm btn-danger" onclick="delete_akses('.$id.')"><i class="fa fa-trash"></i></button>';
        })
        ->rawColumns(['fitur', 'opsi'])
        ->make(true);
    }

    public function akses_form()
    {
        return view('setting.akses.form');
    }

    public function list_akses()
    {
        $akses = DB::table('akses')->get();

        return Datatables::of($akses)
        ->addIndexColumn()
        ->addColumn('opsi', function ($akses) {
            $id = $akses->id;
            $nama = "'".$akses->nama."'";

            return '<button class="btn btn-sm btn-primary" onclick="select_akses('.$id.', '.$nama.')">Pilih</button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function list_fitur()
    {
        $fitur = DB::table('menu as a')
                    ->leftJoin('menu as b', 'a.parent_id', '=', 'b.id')
                    ->select('a.id', 'a.nama', 'a.icon', 'a.route', 'b.nama as parent')
                    ->get();

        return Datatables::of($fitur)
        ->addIndexColumn()
        ->addColumn('opsi', function ($fitur) {
            $id = $fitur->id;
            $nm = "'".$fitur->nama."'";
            return '<button class="btn btn-sm btn-primary" onclick="select_fitur('.$id.', '.$nm.')"> Pilih</button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function akses_save(Request $req)
    {
        $id_akses = $req->_idAkses;
        $nm_akses = $req->_nmAkses;
        $id_fitur = $req->_idFitur;

        $data_akses = [
            'nama' => $nm_akses
        ];

        if (!isset($id_akses)) {
            $return_id = DB::table('akses')->insertGetId($data_akses);
            $id_akses = $return_id;
            $item = [
                'id_akses' => $id_akses,
                'id_menu' => $id_fitur
            ];
            
            $insert_detail = DB::table('akses_detail')->insert($item);
            $res = [
                'code' => 200,
                'msg' => 'Berhasil disimpan',
                'id_akses' => $id_akses
            ];
        }elseif (isset($id_akses)) {
            $item = [
                'id_akses' => $id_akses,
                'id_menu' => $id_fitur
            ];

            $update = DB::table('akses')->where('id', $id_akses)->update($data_akses);
            if (isset($update)) {
                $insert_detail = DB::table('akses_detail')->insert($item);
                $res = [
                    'code' => 201,
                    'msg' => 'Berhasil diupdate',
                    'id_akses' => $id_akses
                ];
            }else {
                $res = [
                    'code' => 400,
                    'msg' => 'Gagal diupdate',
                    'id_akses' => NULL
                ];
            }
        }else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal disimpan',
                'id_akses' => NULL
            ];
        }

        return response()->json($res);
    }

    public function akses_delete(Request $req)
    {
        $delete = DB::table('akses')->where('id', $req->_id)->delete();
        $delete_item = DB::table('akses_detail')->where('id_akses', $req->_id)->delete();

        if ($delete) {
            $res = [
                'code' => 300,
                'msg' => 'Berhasil dihapus'
            ];
        }else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }

        return response()->json($res);
    }

    public function column_fitur($id)
    {
        $fitur = DB::table('akses_detail as a')
                    ->where('a.id_akses', $id)
                    ->leftJoin('menu as b', 'a.id_menu', '=', 'b.id')
                    ->leftJoin('menu as c', 'b.parent_id', '=', 'c.id')
                    ->select('a.id', 'b.nama', 'c.nama as parent')
                    ->get();

        $dt = [];
        foreach ($fitur as $key => $value) {
            $pr = isset($value->parent) ? ' / '.$value->parent : '';
            $dt[] = '<li>'.$value->nama.$pr.'</li>';
        }
        // dd($dt);
        $dt = implode('', $dt);

        return $dt;
    }

    public function item_fitur(Request $req)
    {
        $id_akses = $req->_idAkses;

        $fitur = DB::table('akses_detail as a')
                    ->where('a.id_akses', $id_akses)
                    ->leftJoin('menu as b', 'a.id_menu', '=', 'b.id')
                    ->leftJoin('menu as c', 'b.parent_id', '=', 'c.id')
                    ->select('a.id', 'b.nama', 'c.nama as parent')
                    ->get();

        return Datatables::of($fitur)
        ->addIndexColumn()
        ->addColumn('opsi',  function ($fitur) {
            $id = $fitur->id;
            return '<button class="btn btn-sm btn-danger" onclick="delete_fitur('.$id.')"><i class="fa fa-trash"></i></button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function delete_fitur(Request $req)
    {
        $delete = DB::table('akses_detail')->where('id', $req->_id)->delete();

        if ($delete) {
            $res = [
                'code' => 500,
                'msg' => 'Berhasil dihapus'
            ];
        }else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }

        return response()->json($res);
    }
}
