<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;

class KendaraanController extends Controller
{
    public function __construct ()
    {
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        return view('admin.master.kendaraan.index');
    }

    public function datatable()
    {
        $data = DB::table('kendaraan')->where('status', NULL)->get();
        return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data) {
            $edit = route('kendaraan.form_edit', [base64_encode($data->id)]);
            $id_kendaraan = "'".base64_encode($data->id)."'";
            return '<a href="'.$edit.'" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                    <button type="button" class="btn btn-sm btn-danger" onclick="delete_kendaraan('.$id_kendaraan.')"><i class="fa fa-trash"></i></button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function form()
    {
        return view("admin.master.kendaraan.form");
    }

    public function save(Request $req)
    {
        $id_user = session::get('id_user');
        $kendaraan = $req->kendaraan;
        $no_plat = $req->no_plat;
        $kir = $req->kir;
        $tahunan = $req->tahunan;
        $lima_tahunan = $req->lima_tahunan;
        $penyusutan = $req->penyusutan;
        $data_kendaraan = [
                            "nama" => $kendaraan,
                            "no_plat" => $no_plat,
                            "kir" => $kir,
                            "tahunan" => $tahunan,
                            "lima_tahunan" => $lima_tahunan,
                            "penyusutan" => $penyusutan,
                            "created_at" => date("Y-m-d H:i:s"),
                            "user_add" => $id_user
        ];
        $insert_satuan = DB::table('kendaraan')->insert($data_kendaraan);
        if ($insert_satuan) {
            $res = [
                'code' => 300,
                'msg' => 'Berhasil disimpan'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal Disimpan'
            ];
        }

        $data['response'] = $res;
        return redirect()->route('kendaraan.index')->with($data);
    }

    public function form_edit($id)
    {
        $id_kendaraan = base64_decode($id);
        $kendaraan = DB::table('kendaraan')->where('id',$id_kendaraan)->first();

        $data['id_kendaraan'] = base64_encode($kendaraan->id);
        $data['kendaraan'] = $kendaraan->nama;
        $data['no_plat'] = $kendaraan->no_plat;
        $data['kir'] = $kendaraan->kir;
        $data['tahunan'] = $kendaraan->tahunan;
        $data['lima_tahunan'] = $kendaraan->lima_tahunan;
        $data['penyusutan'] = $kendaraan->penyusutan;

        return view('admin.master.kendaraan.form_edit')->with($data);
    }

    public function update(Request $req)
    {
        $id_user = session::get('id_user');
        $id_kendaraan = base64_decode($req->id_kendaraan);
        $kendaraan = $req->kendaraan;
        $no_plat = $req->no_plat;
        $kir = $req->kir;
        $tahunan = $req->tahunan;
        $lima_tahunan = $req->lima_tahunan;
        $penyusutan = $req->penyusutan;
        
        $data_kendaraan = [
            'nama' => $kendaraan,
            'no_plat' => $no_plat,
            'kir' => $kir,
            'tahunan' => $tahunan,
            'lima_tahunan' => $lima_tahunan,
            'penyusutan' => $penyusutan,
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user
        ];

        $update = DB::table('kendaraan')->where('id', $id_kendaraan)->update($data_kendaraan);
        if ($update) {
            $res = [
                'code' => 201,
                'msg' => 'Data telah diupdate'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal diupdate'
            ];
        }
        $data['response'] = $res;
        return redirect()->route('kendaraan.index')->with($data);
        // return "update";
    }

    public function delete(Request $req)
    {
        $id_user = session::get('id_user');
        $id_kendaraan = base64_decode($req->_idKendaraan);
        
        // dd($id_user);
        $data_kendaraan = [
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user,
            'status' => 9
        ];
        $res = [];
        $update = DB::table('kendaraan')->where('id',$id_kendaraan)->update($data_kendaraan);
        if ($update) {
            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }
}
