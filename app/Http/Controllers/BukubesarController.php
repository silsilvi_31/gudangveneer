<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use File;
use Excel;
use App\Exports\BukubesarExport;

class BukubesarController extends Controller
{
    public function __construct ()
    {
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        return view('admin.bukubesar.index');
    }

    public function total($data)
    {
        $total = 0;
        foreach ($data as $value) {
            $total += $value->total;            
        }
        return $total;
    }

    public function get_jurnal($jurnal, $no_akun)
    {
        $dty = array_filter($jurnal->toArray(), function ($value) use ($no_akun) {
           return $value->no_akun == $no_akun;
        });

        return $dty;
    }

    public function datatable(Request $req)
    {
        $tgl = date('Y-m-d', strtotime($req->_tgl));
        $tgl_dua = date('Y-m-d', strtotime($req->_tglDua));

        $parent_jurnal = DB::table('parent_jurnal')
                                ->where('status', 'tutup')
                                ->orderBy('created_at', 'DESC')
                                ->first();

        $tgl_akhir = isset($parent_jurnal) ? $parent_jurnal->tgl_akhir : '';

        $jurnalQ = DB::table('jurnal')
                            ->whereDate('tgl', '<=', $tgl_akhir)
                            ->get();

        $id_jurnalQ = [];
        foreach ($jurnalQ as $value) {
            $id_jurnalQ[] = $value->id;
        }

        $jurnal = DB::table('jurnal')
                                ->whereBetween('tgl', [$tgl, $tgl_dua])
                                ->whereNotIn('id', $id_jurnalQ)
                                ->get();

        $bukubesar = DB::table('akun')
                            ->whereIn('parent_id', ['1', '2', '3'])
                            ->orderBy('no_akun')
                            ->get();

        $dt = [];
        $tt_debit = 0;
        $tt_kredit = 0;

        foreach ($bukubesar as $value) {
            $dt[] = (object) [
                    'no_akun' => $value->no_akun,
                    'akun' => $value->akun,
                    'jurnal' => $this->get_jurnal($jurnal, $value->no_akun)
            ];
        }
        
        $dt_jurnal = [];

        $debit_child = 0;
        $kredit_child = 0;
        $debit_child_ = 0;
        $kredit_child_ = 0;
        $total_parent = 0;
        $sisa_child = 0;
        $awal = 0;
        $awal_ = 0;

        $ttl = 0;

        foreach ($dt as $x) {
            $total_parent = 0;
            foreach ($x->jurnal as $z) {
                $debit_child_ = ($z->map == 'd') ? $z->total : 0;
                $kredit_child_ = ($z->map == 'k') ? $z->total : 0;
                $total_parent = $awal_ + $debit_child_ - $kredit_child_;
                $awal_ = $total_parent;
            }

            $awal_ = 0;
            $dt_jurnal[] = (object) [
                'no_akun' => $x->no_akun,
                'nama_akun' => '<button class="btn btn-sm btn-light" type="button" id="'.$x->no_akun.'" value="0" onclick="show_all('.$x->no_akun.')"><i class="fa fa-chevron-down"></i></button><strong>'.$x->no_akun.' - '.$x->akun.'</strong>',
                'jenis_jurnal' => '',
                'nama' => '',
                'keterangan' => '',
                'ref' => '',
                'debit' => '',
                'kredit' => '',
                'total' => '<strong>'.number_format(abs($total_parent), 0, ',', '.').'</strong>',
                'tipe' => 'parent'
            ];

            $awal = 0;
            foreach ($x->jurnal as $y) {
                $debit_child = ($y->map == 'd') ? $y->total : 0;
                $kredit_child = ($y->map == 'k') ? $y->total : 0;
                $sisa_child = $awal + $debit_child - $kredit_child;
                $ttl = $debit_child - $kredit_child;
                $dt_jurnal[] = (object) [
                    'no_akun' => $x->no_akun,
                    'nama_akun' => "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp".date('d-m-Y', strtotime($y->tgl)),
                    'jenis_jurnal' => isset($y->jenis_jurnal) ? $y->jenis_jurnal : '-',
                    'nama' => isset($y->nama) ? $y->nama : '-',
                    'keterangan' => isset($y->keterangan) ? $y->keterangan : '-',
                    'ref' => isset($y->ref) ? $y->ref : $y->bm,
                    'debit' => number_format(abs($debit_child), 0, ',', '.'),
                    'kredit' => number_format(abs($kredit_child), 0, ',', '.'),
                    'total' => number_format(abs($sisa_child), 0, ',', '.'),
                    'tipe' => 'child'
                ];
                $awal = $sisa_child;
            }
        }

        $dataQ['data'] = $dt_jurnal;
        // dd($dataQ);
        return response()->json($dataQ);
    }

    public function akun_list()
    {
        $data = DB::table('akun')->get();
        return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('kelompok', function () {
            return 'kel';
        })
        ->addColumn('opsi', function ($data) {
            $id_akun = "'".base64_encode($data->id)."'";
            $nama_akun = $data->akun;
            return '<button type="button" class="btn btn-sm btn-danger" onclick="delete_jabatan()"><i class="fa fa-trash"></i></button>
                    <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function form()
    {
        return view('admin.master.akun.form');
    }

    // AKUN BARU

    public function formQ()
    {
        return view('admin.master.akun.formQ');
    }

    public function saveQ(Request $req)
    {
        $id_user = session::get('id_user');
        $no_akun = $req->_noAkun;
        $nama_akun = $req->_namaAkun;

        $data = [
            'no_akun' => $no_akun,
            'akun' => $nama_akun,
            'created_at' => date("Y-m-d H:i:s"),
            'user_add' => $id_user
        ];

        $cek_akun = DB::table('akun')->where('no_akun', $no_akun)->first();

        $res = [];
        if (isset($cek_akun)) {
            $res = [
                    "code" => 400,
                    "msg" => "Nomor akun sudah Ada"
            ];
        } else {
            $insert_akun = DB::table('akun')->insert($data);
            if ($insert_akun) {
                $res = [
                    "code" => 300,
                    "msg" => "Data berhasil disimpan"
                ];      
            } else {
                $res = [
                    "code" => 400,
                    "msg" => "Data gagal disimpan"
                ];
            }
        }
        return response()->json($res);
    }

    public function akun_listQ()
    {
        $data = DB::table('akun')->get();
        
        return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('kelompok', function () {
            return 'kel';
        })
        ->addColumn('opsi', function ($data) {
            $id_akun = "'".base64_encode($data->id)."'";
            $no_akun = "'".$data->no_akun."'";
            $nama_akun = "'".$data->akun."'";
            return '<button class="btn btn-sm btn-primary" onclick="select_akun('.$no_akun.','.$nama_akun.')">Pilih</button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function excel_bukubesar($tgl)
    {
        $pecah = explode('&', $tgl);
        $tgl_m = $pecah[0];
        $tgl_a = $pecah[1];  

        $tgl_m_format = date('Y-m-d', strtotime($tgl_m));
        $tgl_a_format = date('Y-m-d', strtotime($tgl_a));

        $jurnal = DB::table('jurnal')
                                ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                                ->get();

        $bukubesar = DB::table('akun')
                            ->whereIn('no_akun', ['110', '120', '130', '210', '220', '410', '610'])
                            ->orderBy('no_akun')
                            ->get();

        $dt = [];
        $tt_debit = 0;
        $tt_kredit = 0;

        foreach ($bukubesar as $value) {
            $dt[] = (object) [
                    'no_akun' => $value->no_akun,
                    'akun' => $value->akun,
                    'jurnal' => $this->get_jurnal($jurnal, $value->no_akun)
            ];
        }
        
        $dt_jurnal = [];

        foreach ($dt as $x) {
            $dt_jurnal[] = (object) [
                'nama_akun' => $x->no_akun.' - '.$x->akun,
                'jenis_jurnal' => '',
                'keterangan' => '',
                'ref' => '',
                'debit' => '',
                'kredit' => '',
                'total' => ''
            ];

            foreach ($x->jurnal as $y) {
                $dt_jurnal[] = (object) [
                    'nama_akun' => date('d-m-Y', strtotime($y->tgl)),
                    'jenis_jurnal' => $y->jenis_jurnal,
                    'keterangan' => $y->keterangan,
                    'ref' => $y->ref,
                    'debit' => ($y->map == 'd') ? $y->harga : '-',
                    'kredit' => ($y->map == 'k') ? $y->harga : '-',
                    'total' => '-'
                ];
            }
        }

        $dt['rekap'] = $dt_jurnal;
        $dt['tgl_m'] = $tgl_m;
        $dt['tgl_a'] = $tgl_a;

        $nama_file = "Rekap Buku Besar ".$tgl_m."-".$tgl_a.".xlsx";
        return Excel::download(new BukubesarExport($dt), $nama_file); 
    }

}