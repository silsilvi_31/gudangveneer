<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use Excel;
use App\User;
use App\Exports\UserExport;
use App\Exports\PresensiExport;

class KaryawanController extends Controller
{
    public function __construct ()
    {
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        return view('admin.manage.karyawan.index');
    }
    
    public function datatable()
    {
        $data = DB::table('karyawan as a')
                ->leftJoin('jabatan as b', 'a.id_jabatan', '=', 'b.id')
                ->select('a.kode', 'a.nama', 'a.alamat', 'a.no_telp', 'a.id_jabatan', 'b.jabatan')
                ->get();
        
        return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data) {
            $kode = $data->kode;
            $nama = $data->nama;
            $id_jabatan = $data->id_jabatan;
            $alamat = $data->alamat;
            $no_telp = $data->no_telp;
            return '<button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-edit" data-form="edit" data-kode="'.$kode.'" data-nama="'.$nama.'" data-id_jabatan="'.$id_jabatan.'" data-alamat="'.$alamat.'" data-no_telp="'.$no_telp.'"><i class="fa fa-fw fa-edit"></i></button>
                    <button type="button" class="btn btn-sm btn-danger" onclick="delete_karyawan('.$kode.')"><i class="fa fa-fw fa-trash"></i></button>';
        })
        ->rawColumns(['opsi'])
        ->make(true); 
    }

    public function save(Request $req)
    {
        $id_users = session::get('id_user');
        $kode = $req->_kode; 
        $nama = $req->_nama;
        $id_jabatan = $req->_idJabatan;
        $alamat = $req->_alamat;
        $no_telp = $req->_noTelp;

        $cek_karyawan = DB::table('karyawan')->where('kode', $kode)->first();
        
        if (is_null($cek_karyawan)) {
            $data_karyawan = [
                'kode' => $kode,
                'nama' => $nama,
                'id_jabatan' => $id_jabatan,
                'alamat' => $alamat,
                'no_telp' => $no_telp,
                'user_add' => $id_users,
                'created_at' => date("Y-m-d H:i:s")
            ];

            $insert_karyawan = DB::table('karyawan')->insert($data_karyawan);
            if ($insert_karyawan) {
                $res = [
                    'code' => 200,
                    'msg' => 'Data karyawan berhasil ditambah'
                ];
            }else {
                $res = [
                    'code' => 400,
                    'msg' => 'Data karyawan gagal ditambah !'
                ];
            }
        }elseif (!is_null($cek_karyawan)) {
            $data_karyawan = [
                'nama' => $nama,
                'id_jabatan' => $id_jabatan,
                'alamat' => $alamat,
                'no_telp' => $no_telp,
                'user_upd' => $id_users,
                'updated_at' => date("Y-m-d H:i:s")
            ];
            
            $update_karyawan = DB::table('karyawan')
                                ->where('kode', $kode)
                                ->update($data_karyawan);
            
            if ($update_karyawan) {
                $res = [
                    'code' => 201,
                    'msg' => 'Data karyawan berhasil diupdate'
                ];
            }else {
                $res = [
                    'code' => 400,
                    'msg' => 'Data karyawan gagal diupdate !'
                ];
            }
        }
        return response()->json($res);
    }

    public function delete(Request $req)
    {
        $kode = $req->_kode;
        $delete = DB::table('karyawan')->where('kode', $kode)->delete();

        if ($delete) {
            $res = [
                'code' => 300,
                'msg' => 'Data karyawan berhasil dihapus'
            ];
        }else {
            $res = [
                'code' => 400,
                'msg' => 'Data karyawan gagal diupdate !'
            ];
        }

        return response()->json($res);
    }
}