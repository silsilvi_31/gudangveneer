<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;

class SampleController extends Controller
{
    public function __construct ()
    {
        date_default_timezone_set("Asia/Jakarta");    
    }

    public function form()
    {
        return view('admin.master.sample.form');
    }

    public function sopir(Request $req)
    {
        // return $req->all();
        // $nama = $req->all()['query']; 
        $search = $req->get('term');
        $sopir = DB::table('karyawan')
                    ->where('id_jabatan', 4)
                    ->where('nama', 'LIKE', '%'.$search.'%')
                    ->get();
       
        return response()->json($sopir);
        // return $req->all();
        // $data = [
        //     (object)['value' => 1, 'data' => 'budi'],
        //     (object)['value' => 2, 'data' => 'budis']
        // ];

        // return response()->json($data);
    }

    public static function detail_menu($id)
    {
        $data = DB::table('menu')->where('id', $id)->first();

        return $data;
    }

    public function menu_q()
    {
        $akses = DB::table('akses_detail as a')
                ->where('a.id_akses', 22)
                ->leftJoin('menu as b', 'a.id_menu', '=', 'b.id')
                ->select('b.id', 'b.nama', 'b.icon', 'b.route', 'b.parent_id')
                ->get();

        // dd($akses);

        $dt = [];
        $no = 0;
        foreach ($akses as $key => $e) {
            if (!isset($e->parent_id)) {
                $dt[$this->detail_menu($e->id)->nama] = [
                                'nama' => $this->detail_menu($e->id)->nama, 
                                'icon' => $this->detail_menu($e->id)->icon,
                                'route' => $this->detail_menu($e->id)->route,
                                'child' => NULL 
                            ];        
            }else {
                $dt[$this->detail_menu($e->parent_id)->nama]['nama'] = $this->detail_menu($e->parent_id)->nama;
                $dt[$this->detail_menu($e->parent_id)->nama]['icon'] = $this->detail_menu($e->parent_id)->icon;
                $dt[$this->detail_menu($e->parent_id)->nama]['route'] = $this->detail_menu($e->parent_id)->route;
                $dt[$this->detail_menu($e->parent_id)->nama]['child'][] = $e;
            }
        }

        $bulan = array('Januari', 'Februari', 'Maret');
        $kendaraan = array('jenis' => 'Mobil', 'merk' => 'Toyota', 'tipe' => 'Vios');
        $fert = [];
        foreach ($bulan as $k) {
            $fert[] = $k;
        }

        $kd = [];
        foreach ($kendaraan as $key => $a) {
            $kd[] = $a;
        }
        $mmm = [];
        foreach ($dt as $key => $e) {
            $mmm[] = $e;
        }
        // $data['op'] = $dt;
        // dd($dt['menu']);
        // $data['menu'] = $dt;
        // $data['bulan'] = $bulan;
        // $data['ds'] = $kendaraan;
        // $data['dss'] = $kd;
        // $data['hs'] = $fert;
        $data['hs_m'] = $mmm;
        // dd($data);
        
        return view('master.ella_v2')->with($data);
    }
}
